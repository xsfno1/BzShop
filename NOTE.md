百战商城项目（微服务版的Ego项目）

一、 遇到的坑：
1.generatorSqlmapCustom --- MyBatis逆向工程生成
    异常 ：Result Maps collection already contains value for com.bjsxt.mapper.TbItemMap...
    原因 ：生成Mapper.xml代码时重复生成
    解决 ： 使用本地数据库时会出现这种问题，而使用Linux中的MySQL则不会出现这问题。
 
2.generatorSqlmapCustom --- MyBatis逆向工程生成
    异常 ：无法生成selectByPrimaryKey、deleteByPrimaryKey、updateByPrimaryKey MySQL
    解决 ：在jdbcConnection节点里配置useInformationSchema属性，可以解决mybatis-generator不识别主键问题：
        <jdbcConnection driverClass="com.mysql.jdbc.Driver"    connectionURL="jdbc:mysql://localhost:3306/test"
                 userId="root" password="rootroot">
             <!--设置为 true 可以获取 tables 信息, 解决生成文件缺少 xxxByPrimaryKey 的问题 -->
             <property name="useInformationSchema" value="true"/>
        </jdbcConnection>

3.Druid连接池连接问题
    异常 ：java.sql.SQLException: validateConnection false
    原因 ：版本问题
    解决 ：将Druid版本调整为1.0.15
    
4.声明式服务调用Feign
    异常 ：The bean 'common-item.FeignClientSpecification', defined in null, could not be registered. A bean with that name has already been defined in null and overriding is disabled.
    原因 ：@FeignClient("common-item")中名称重复，在SpringBoot 2.1之后不允许重复
    解决 ：spring.main.allow-bean-definition-overriding=true
           ##在SpringBoot 2.1之前默认为true
           ##更新应该有它的道理，所以有条件的话可以满足它的需求

5.服务网关Zuul 与 Hystrix-Dashboard监控 的无缝结合
    异常 ：localhost:9000/hystrix.stream 无法访问
    原因 ：SpringCloud1.x版本下访问地址是 ip:port/hystrix.stream
           而SpringCloud2.x 版本下访问地址为 ip:port/actuator/hystrix.stream

6.配置中心Config + 消息总线Bus
    异常 ：无法通过消息总线更新配置，但可以通过普通方式更新
    原因 ：(无解)通过 HttpClientUtil 类更新失败，而通过 PostMan 软件更新成功
            Url ：http://localhost:9001/actuator/bus-refresh?destination=backend-item:**

7.配置中心SpringCloudConfig使用用户认证SpringBootSecurity后
    参考解决文章 ：https://blog.csdn.net/yakson/article/details/80876955

8.新版本的 SpringCloudConfig:Greenwich.SR2 对称加密
    异常 ： http://localhost:9001/encrypt/status 检验加密状态失败
    解决 ： 1.配置文件必须为 bootstrap.yml/bootstrap.properties
            2.配置文件中添加：spring.cloud.config.server.bootstrap=true
            加密 ：http://127.0.0.1:9001/encrypt
            解密 ：http://127.0.0.1:9001/decrypt

9.(重要！！！重要！！！重要！！！重要！！！重要！！！重要！！！)Vue部署Linux Nginx跨域问题
    异常 ：跨域失败
    解决 ：在 nginx.conf 配置文件入手，关键点 try_files $uri /index.html;
            location / {
                root   bz-store;
                try_files $uri /index.html;
            }
            location /api/ {
                proxy_pass   http://192.168.255.145:9030/;
            }


二、项目笔记
1.@EnableDiscoveryClient 与 @EnableEurekaClient 区别
    @EnableDiscoveryClient : 服务发现，可以发现Zookeeper和Eureka等等服务注册中心
    @EnableEurekaClient    : 服务发现，只能发现Eureka服务注册中心

2.优雅停服（暂时只实现对所有服务进行刷新）
    https://blog.csdn.net/weixin_33869377/article/details/92271385
    * 步骤1:修改POM文件添加 actuator 坐标
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
    * 步骤2:修改全局配置文件，配置启用shutdown
        endpoints.shutdown.enabled=true
    * 步骤3:POST请求
        http://127.0.0.1:9001/actuator/shutdown

3.为什么使用Redis生成订单ID策略？
    答：首先，Redis是单进程，所以Redis自增长生成ID是绝对不会重复的。
        然后，通过IdUtils工具类生成ID有极小概率会重复。
        最后，订单ID不重复要求高，所以就选用Redis自增长ID生成策略。

3.Zuul实现限流方式：利用谷歌令牌桶算法实现限流

4.通过 Hystrix 对下游服务做降级处理
    问：为什么已经有Zuul的降级处理，还需要对下游服务再做降级处理呢？
    答：因为Zuul只是为上游服务提供降级处理，对下游服务并无法处理到。
    * 在需要的下游服务上游服务进行以下步骤:（注意：是在上游服务中配置！上游服务中配置！上游服务中配置！）
    * 步骤1:创建返回托底数据对象
        实现 FallbackFactory 接口
    * 步骤2:修改 feignClient,添加 @FeignClient(...,fallbackFactory=xxx.class)
    * (重点注意)步骤3:(重点注意LCN)做降级处理后，表示不会再出现异常，那么LCN的分布式事务就是出问题，
            因为LCN事务机制就是根据异常回滚的。
            处理：在LCN所作用的代码中手动抛出异常。（例如 backend-item服务中的BackendItemServiceImpl）
    * 步骤3:修改配置文件，开启Feign对Hystrix的支持,默认为false
          feign.hystrix.enabled=true