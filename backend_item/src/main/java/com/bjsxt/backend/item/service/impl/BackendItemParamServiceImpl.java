package com.bjsxt.backend.item.service.impl;

import com.bjsxt.backend.item.feign.CommonItemParamFeignClient;
import com.bjsxt.backend.item.service.BackendItemParamService;
import com.bjsxt.entity.PageResult;
import com.bjsxt.entity.Result;
import com.bjsxt.pojo.TbItemParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;

@Service
public class BackendItemParamServiceImpl implements BackendItemParamService {
    @Autowired
    private CommonItemParamFeignClient commonItemParamFeignClient;
    /**
     * 查询商品规格参数模板
     * @param itemCatId 商品分类ID
     */
    @Override
    public Result selectItemParamByItemCatId(Long itemCatId) {
        TbItemParam tbItemParam = this.commonItemParamFeignClient.selectItemParamByItemCatId(itemCatId);
        if(!StringUtils.isEmpty(tbItemParam)){
            return Result.ok(tbItemParam);
        }
        return Result.error("查无结果");
    }
    /**
     * 规格参数查询接口
     * @param page 当前页
     * @param rows 页记录数
     */
    @Override
    public Result selectItemParamAll(Integer page, Integer rows) {
        PageResult pageResult = this.commonItemParamFeignClient.selectItemParamAll(page,rows);
        if(pageResult != null && pageResult.getResult().size() > 0){
            return Result.ok(pageResult);
        }
        return Result.error("查无结果");
    }
    /**
     * 规格参数添加接口
     * @param itemCatId 分类主键
     * @param paramData 规格参数数据
     */
    @Override
    public Result insertItemParam(Long itemCatId, String paramData) {
        Date date = new Date();
        //完善对象信息
        TbItemParam tbItemParam = new TbItemParam();
        tbItemParam.setItemCatId(itemCatId);
        tbItemParam.setParamData(paramData);
        tbItemParam.setCreated(date);
        tbItemParam.setUpdated(date);

        Integer reslut = this.commonItemParamFeignClient.insertItemParam(tbItemParam);
        if(reslut != null && reslut > 0){
            return Result.ok("添加成功");
        }

        return Result.error("添加失败");
    }
    /**
     * 规格参数删除接口
     * @param id 主键
     */
    @Override
    public Result deleteItemParamById(Long id) {
        Integer reslut = this.commonItemParamFeignClient.deleteItemParamById(id);
        if(reslut != null && reslut > 0){
            return Result.ok();
        }
        return Result.error("删除失败");
    }
}
