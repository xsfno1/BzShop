package com.bjsxt.backend.item.feign;

import com.bjsxt.backend.item.fallback.CommonItemFeignClientFallbackFactory;
import com.bjsxt.feign.CommonItemCatFeignService;
import com.bjsxt.feign.CommonItemFeignService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "common-item" , fallbackFactory = CommonItemFeignClientFallbackFactory.class)
public interface CommonItemFeignClient extends CommonItemFeignService {
}
