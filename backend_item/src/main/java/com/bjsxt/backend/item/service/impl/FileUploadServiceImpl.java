package com.bjsxt.backend.item.service.impl;

import com.bjsxt.backend.item.service.FileUploadService;
import com.bjsxt.entity.Result;
import com.bjsxt.utils.FtpUtil;
import com.bjsxt.utils.IDUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
@RefreshScope   //配置中心刷新作用域
public class FileUploadServiceImpl implements FileUploadService {
    @Value("${FTP_HOST}")
    private String FTP_HOST;
    @Value("${FTP_PORT}")
    private int FTP_PORT;
    @Value("${FTP_USERNAME}")
    private String FTP_USERNAME;
    @Value("${FTP_PASSWORD}")
    private String FTP_PASSWORD;
    @Value("${FTP_BASEPATH}")
    private String FTP_BASEPATH;
    @Value("${IMAGE_HTTP_PATH}")
    private String IMAGE_HTTP_PATH;

    @Override
    public Result fileUpload(MultipartFile file) {
        try {
            //定义上传图片的目录结构
            SimpleDateFormat format = new SimpleDateFormat("/yyyy/MM/dd/");
            String path = format.format(new Date());

            //设置新的文件名
            String newFilename =
                    IDUtils.genImageName() + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));

            FtpUtil.uploadImg(this.FTP_HOST, this.FTP_PORT, this.FTP_USERNAME, this.FTP_PASSWORD, this.FTP_BASEPATH,
                    path, newFilename, file.getInputStream());

            //图片保存路径
            String imageURL = this.IMAGE_HTTP_PATH + path + newFilename;

            return Result.ok(imageURL);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.error("图片上传失败");
    }
}
