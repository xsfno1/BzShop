package com.bjsxt.backend.item.controller;

import com.bjsxt.backend.item.service.BackendItemService;
import com.bjsxt.entity.Result;
import com.bjsxt.pojo.TbItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/backend/item", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@RefreshScope   //配置中心刷新作用域
public class BackendItemController {
    private Logger logger = LoggerFactory.getLogger(BackendItemController.class);

    @Autowired
    private BackendItemService itemService;
    // ###################    测试配置中心(只为测试  并无实际作用)     #####################################
    @Value("${XSF_AND_GEM}")
    private String XSF_AND_GEM;
    // ###################    测试配置中心(只为测试  并无实际作用)     #####################################
    /**
     * 分页查询商品列表
     * @param page 当前页
     * @param rows 每页显示数
     * @return Result
     */
    @GetMapping("/selectTbItemAllByPage")
    public Result selectTbItemAllByPage(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue =
            "10") Integer rows) {
        try {
            // ###################    测试配置中心(只为测试  并无实际作用)     #####################################
            System.out.println("###################    测试配置中心  : "+this.XSF_AND_GEM);
            // ###################    测试配置中心(只为测试  并无实际作用)     #####################################
            return this.itemService.selectTbItemAllByPage(page, rows);
        } catch (Exception e) {
            e.printStackTrace();
            this.logger.error(e.getMessage());
        }
        return Result.build(500, "error");
    }

    /**
     * 商品添加接口
     * @param tbItem     商品
     * @param desc       商品详情
     * @param itemParams 商品规格参数
     */
    @GetMapping("/insertTbItem")
    public Result insertTbItem(TbItem tbItem, String desc, String itemParams) {
        try {
            return this.itemService.insertTbItem(tbItem, desc, itemParams);
        } catch (Exception e) {
            e.printStackTrace();
            this.logger.error(e.getMessage());
        }
        return Result.build(500, "error");
    }

    /**
     * 根据商品主键实现商品删除 ：将状态更新为 3-删除
     * @param itemId 商品主键
     */
    @PostMapping("/deleteItemById")
    public Result deleteItemById(Long itemId) {
        try {
            return this.itemService.deleteItemById(itemId);
        } catch (Exception e) {
            e.printStackTrace();
            this.logger.error(e.getMessage());
        }
        return Result.build(500, "error");
    }

    /**
     * 预更新商品接口
     * @param itemId 商品ID
     */
    @PostMapping("/preUpdateItem")
    public Result preUpdateItem(Long itemId) {
        try {
            return this.itemService.preUpdateItem(itemId);
        } catch (Exception e) {
            e.printStackTrace();
            this.logger.error(e.getMessage());
        }
        return Result.build(500, "error");
    }
    /**
     * 修改商品信息接口
     * @param tbItem 商品对象
     * @param desc 商品详情
     * @param itemParams 商品规格参数
     */
    @GetMapping("/updateTbItem")
    public Result updateTbItem(TbItem tbItem , String desc , String itemParams) {
        try {
            return this.itemService.updateTbItem(tbItem,desc,itemParams);
        } catch (Exception e) {
            e.printStackTrace();
            this.logger.error(e.getMessage());
        }
        return Result.build(500, "error");
    }

}

