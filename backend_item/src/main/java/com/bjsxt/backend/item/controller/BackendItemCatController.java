package com.bjsxt.backend.item.controller;

import com.bjsxt.backend.item.service.BackendItemCatService;
import com.bjsxt.entity.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/backend/itemCategory", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class BackendItemCatController {
    @Autowired
    private BackendItemCatService backendItemCatService;

    /**
     * 根据类目ID 查询子类目
     * @param id 父节点ID，默认0
     * @return Result
     */
    @PostMapping("/selectItemCategoryByParentId")
    public Result selectItemCategoryByParentId(@RequestParam(defaultValue = "0") Long id){
        return this.backendItemCatService.selectItemCategoryByParentId(id);
    }
}
