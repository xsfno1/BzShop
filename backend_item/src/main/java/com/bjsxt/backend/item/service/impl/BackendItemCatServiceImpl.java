package com.bjsxt.backend.item.service.impl;

import com.bjsxt.backend.item.feign.CommonItemCatFeignClient;
import com.bjsxt.backend.item.service.BackendItemCatService;
import com.bjsxt.entity.Result;
import com.bjsxt.pojo.TbItemCat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BackendItemCatServiceImpl implements BackendItemCatService {
    @Autowired
    private CommonItemCatFeignClient commonItemCatFeignClient;

    @Override
    public Result selectItemCategoryByParentId(Long id) {
        List<TbItemCat> list = this.commonItemCatFeignClient.selectItemCategoryByParentId(id);
        if(list != null && list.size() > 0){
            return Result.ok(list);
        }
        return Result.error("查无结果");
    }
}
