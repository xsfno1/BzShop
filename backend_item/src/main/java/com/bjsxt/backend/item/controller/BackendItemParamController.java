package com.bjsxt.backend.item.controller;

import com.bjsxt.backend.item.service.BackendItemParamService;
import com.bjsxt.entity.Result;
import com.bjsxt.pojo.TbItemParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/backend/itemParam", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class BackendItemParamController {
    @Autowired
    private BackendItemParamService backendItemParamService;

    @GetMapping("/selectItemParamByItemCatId/{itemCatId}")
    public Result selectItemParamByItemCatId(@PathVariable Long itemCatId) {
        try {
            return this.backendItemParamService.selectItemParamByItemCatId(itemCatId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.build(500, "error");
    }

    /**
     * 规格参数查询接口
     * @param page 当前页
     * @param rows 页记录数
     */
    @GetMapping("/selectItemParamAll")
    public Result selectItemParamAll(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue =
            "30") Integer rows) {
        try {
            return this.backendItemParamService.selectItemParamAll(page,rows);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.build(500, "error");
    }
    /**
     * 规格参数添加接口
     * @param itemCatId 分类主键
     * @param paramData 规格参数数据
     */
    @PostMapping("/insertItemParam")
    public Result insertItemParam(Long itemCatId ,String paramData) {
        try {
            return this.backendItemParamService.insertItemParam(itemCatId,paramData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.build(500, "error");
    }
    /**
     * 规格参数删除接口
     * @param id 主键
     */
    @GetMapping("/deleteItemParamById")
    public Result deleteItemParamById(Long id) {
        try {
            return this.backendItemParamService.deleteItemParamById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.build(500, "error");
    }
}
