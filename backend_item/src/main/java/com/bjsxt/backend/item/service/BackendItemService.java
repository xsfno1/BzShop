package com.bjsxt.backend.item.service;

import com.bjsxt.entity.Result;
import com.bjsxt.pojo.TbItem;

public interface BackendItemService {
    Result selectTbItemAllByPage(Integer page, Integer rows);

    Result insertTbItem(TbItem tbItem, String desc, String itemParams);

    Result deleteItemById(Long itemId);

    Result preUpdateItem(Long itemId);

    Result updateTbItem(TbItem tbItem, String desc, String itemParams);
}
