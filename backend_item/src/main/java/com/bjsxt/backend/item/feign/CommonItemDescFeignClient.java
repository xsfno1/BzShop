package com.bjsxt.backend.item.feign;

import com.bjsxt.feign.CommonItemDescFeignService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("common-item")
public interface CommonItemDescFeignClient extends CommonItemDescFeignService {
}
