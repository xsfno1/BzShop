package com.bjsxt.backend.item.feign;

import com.bjsxt.feign.CommonItemParamFeignService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("common-item")
public interface CommonItemParamFeignClient extends CommonItemParamFeignService {
}
