package com.bjsxt.backend.item.service.impl;

import com.bjsxt.backend.item.feign.CommonItemFeignClient;
import com.bjsxt.backend.item.service.BackendItemService;
import com.bjsxt.entity.PageResult;
import com.bjsxt.entity.Result;
import com.bjsxt.feign.CommonItemDescFeignService;
import com.bjsxt.feign.CommonItemParamItemFeignService;
import com.bjsxt.pojo.TbItem;
import com.bjsxt.pojo.TbItemDesc;
import com.bjsxt.pojo.TbItemParamItem;
import com.bjsxt.utils.IDUtils;
import com.codingapi.txlcn.tc.annotation.LcnTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

@Service
public class BackendItemServiceImpl implements BackendItemService {
    @Autowired
    private CommonItemFeignClient commonItemFeignClient;
    @Autowired
    private CommonItemDescFeignService commonItemDescFeignService;
    @Autowired
    private CommonItemParamItemFeignService commonItemParamItemFeignService;

    /**
     * 分页查询商品列表
     * @param page 当前页
     * @param rows 每页显示数
     * @return PageResult
     */
    @Override
    public Result selectTbItemAllByPage(Integer page, Integer rows) {
        PageResult pageResult = this.commonItemFeignClient.selectTbItemAllByPage(page, rows);

        if (pageResult != null && pageResult.getResult() != null &&
                pageResult.getResult().size() > 0) {
            return Result.ok(pageResult);
        }
        return Result.error("查无结果");
    }

    /**
     * 商品添加接口
     *
     * @param tbItem     商品
     * @param desc       商品详情
     * @param itemParams 商品规格参数
     */
    @Override
    @LcnTransaction
    public Result insertTbItem(TbItem tbItem, String desc, String itemParams) {
        //补齐 Tbitem 数据
        Long itemId = IDUtils.genItemId();
        Date date = new Date();
        tbItem.setId(itemId);
        tbItem.setStatus((byte) 1);
        tbItem.setUpdated(date);
        tbItem.setCreated(date);
        Integer result1 = this.commonItemFeignClient.insertTbItem(tbItem);

        //补齐商品描述对象
        TbItemDesc tbItemDesc = new TbItemDesc();
        tbItemDesc.setItemId(itemId);
        tbItemDesc.setItemDesc(desc);
        tbItemDesc.setCreated(date);
        tbItemDesc.setUpdated(date);
        Integer result2 = this.commonItemDescFeignService.insertTbItemDesc(tbItemDesc);

        //补齐商品规格参数
        TbItemParamItem tbItemParamItem = new TbItemParamItem();
        tbItemParamItem.setItemId(itemId);
        tbItemParamItem.setParamData(itemParams);
        tbItemParamItem.setUpdated(date);
        tbItemParamItem.setCreated(date);
        Integer result3 = this.commonItemParamItemFeignService.insertTbItemParamItem(tbItemParamItem);

        /**
         * 做降级处理后，表示不会再出现异常，那么LCN的分布式事务就是出问题，
         * 因为LCN事务机制就是根据异常回滚的。
         * 处理：在LCN所作用的代码中手动抛出异常。
         */
        if(result1 == null || result2 == null || result3 == null) {
            throw new RuntimeException();
        }

        return Result.ok();
    }
    /**
     * 根据商品主键实现商品删除 ：将状态更新为 3-删除
     * @param itemId 商品主键
     */
    @Override
    public Result deleteItemById(Long itemId) {
        TbItem tbItem = new TbItem();
        tbItem.setId(itemId);
        tbItem.setStatus((byte) 3);
        tbItem.setUpdated(new Date());
        Integer result = this.commonItemFeignClient.deleteItemById(tbItem);
        if(result!=null&&result>0){
            return Result.ok("删除成功");
        }
        return Result.error("删除失败");
    }
    /**
     * 预更新商品接口
     * @param itemId 商品ID
     */
    @Override
    public Result preUpdateItem(Long itemId) {
        Map<String, Object> map = this.commonItemFeignClient.preUpdateItem(itemId);
        if(map!=null){
            return Result.ok(map);
        }
        return Result.error("查无结果");
    }
    /**
     * 更新商品： 更新 TbItem 表， 更新 TbitemDesc 表， 更新 TbItempParamItem 表
     * @param tbItem 商品对象
     * @param desc 商品详情
     * @param itemParams 商品规格参数
     */
    @Override
    @LcnTransaction
    public Result updateTbItem(TbItem tbItem, String desc, String itemParams) {
        Date date = new Date();

        //更新商品
        tbItem.setUpdated(date);
        this.commonItemFeignClient.updateTbItem(tbItem);

        //更新商品描述
        TbItemDesc tbItemDesc = new TbItemDesc();
        tbItemDesc.setItemId(tbItem.getId());
        tbItemDesc.setUpdated(date);
        tbItemDesc.setItemDesc(desc);
        this.commonItemDescFeignService.updateTbItemDesc(tbItemDesc);


        TbItemParamItem tbItemParamItem = new TbItemParamItem();
        tbItemParamItem.setParamData(itemParams);
        tbItemParamItem.setItemId(tbItem.getId());
        tbItemDesc.setUpdated(date);
        this.commonItemParamItemFeignService.updateTbItemParamItem(tbItemParamItem);

        return Result.ok();
    }
}

