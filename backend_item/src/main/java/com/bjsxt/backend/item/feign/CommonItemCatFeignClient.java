package com.bjsxt.backend.item.feign;

import com.bjsxt.feign.CommonItemCatFeignService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("common-item")
public interface CommonItemCatFeignClient extends CommonItemCatFeignService {
}
