package com.bjsxt.backend.item.feign;

import com.bjsxt.feign.CommonItemParamItemFeignService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("common-item")
public interface CommonItemParamItemFeignClient extends CommonItemParamItemFeignService {
}
