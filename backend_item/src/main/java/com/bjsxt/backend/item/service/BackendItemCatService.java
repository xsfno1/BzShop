package com.bjsxt.backend.item.service;

import com.bjsxt.entity.Result;

public interface BackendItemCatService {
    Result selectItemCategoryByParentId(Long id);
}
