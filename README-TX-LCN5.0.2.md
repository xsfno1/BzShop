TX-LCN分布式事务框架 (5.0.2.RELEASE)

过往使用的是 TX-LCN4.x ，是基于 SpringBoot1.x
而 TX-LCN5.x 是基于 SpringBoot2.x 以上的。
在 Spring Boot2.x 版本中要求 TX-LCN 必须是 5.0 以上

一、使用步骤（细节可查询有道云笔记《TX-LCN：使用教程》）
    ################    服务端    #########################
    1.引入工程到 IDEA
    2.打开 txlcn-tm 目录，在 /src/main/resources 中有 tx-manager.sql 文件，导入到数据库中
    3.修改 txlcn-tm 目录的 /src/main/resources/application.properties 配置文件
        1.MySQL 地址
            spring.datasource.url=jdbc:mysql://192.168.255.140:3306/tx-manager?characterEncoding=UTF-8
        2.TX-LCN 运行环境地址
            tx-lcn.manager.host=192.168.255.140
        3.Redis 地址
            spring.redis.host=192.168.255.140
    4.Maven 打包 tx-lcn 根目录
    5.将打包好 TX 服务端的 txlcn-tm-5.0.2.RELEASE.zip 上传到 Linux 中
    6.编写启动脚本
    ################    客户端    #########################
    7.创建TX-LCN客户端项目
         <dependency>
            <groupId>com.codingapi.txlcn</groupId>
            <artifactId>txlcn-tc</artifactId>
            <version>5.0.2.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>com.codingapi.txlcn</groupId>
            <artifactId>txlcn-txmsg-netty</artifactId>
            <version>5.0.2.RELEASE</version>
        </dependency>
        <!--MyBatis And Spring Integration Starter-->
        <dependency>
            <groupId>org.mybatis.spring.boot</groupId>
            <artifactId>mybatis-spring-boot-starter</artifactId>
        </dependency>
    8.在服务中使用 TX-LCN 做分布式事务处理
        1.把TX-LCN客户端项目坐标引入所需要的项目中
            注意：TX-LCN客户端依赖于Spring-MyBatis坐标
        2.修改配置文件， 添加 TX-LCN 服务端地址
            tx-lcn.client.manager-address=192.168.70.157:8070
        3.修改启动类， 开启 TX-LCN
            @EnableDistributedTransaction
        4.在方法上添加分布式事务处理注解
            @LcnTransaction