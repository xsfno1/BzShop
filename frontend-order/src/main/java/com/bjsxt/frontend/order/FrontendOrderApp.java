package com.bjsxt.frontend.order;

import com.codingapi.txlcn.tc.config.EnableDistributedTransaction;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@EnableDistributedTransaction   //tx-lcn
@MapperScan("com.bjsxt.mapper")
public class FrontendOrderApp {
    public static void main(String[] args) {
        SpringApplication.run(FrontendOrderApp.class,args);
    }
}
