package com.bjsxt.frontend.order.service.impl;

import com.bjsxt.entity.Result;
import com.bjsxt.frontend.order.feign.CommonRedisOrderFeignClient;
import com.bjsxt.frontend.order.feign.FrontendCartFeignClient;
import com.bjsxt.frontend.order.service.FrontendOrderService;
import com.bjsxt.mapper.TbOrderItemMapper;
import com.bjsxt.mapper.TbOrderMapper;
import com.bjsxt.mapper.TbOrderShippingMapper;
import com.bjsxt.pojo.TbOrder;
import com.bjsxt.pojo.TbOrderItem;
import com.bjsxt.pojo.TbOrderShipping;
import com.bjsxt.utils.IDUtils;
import com.bjsxt.utils.JsonUtils;
import com.codingapi.txlcn.tc.annotation.LcnTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class FrontendOrderServiceImpl implements FrontendOrderService {
    @Autowired
    private CommonRedisOrderFeignClient commonRedisOrderFeignClient;
    @Autowired
    private TbOrderMapper tbOrderMapper;
    @Autowired
    private TbOrderItemMapper tbOrderItemMapper;
    @Autowired
    private TbOrderShippingMapper tbOrderShippingMapper;
    @Autowired
    private FrontendCartFeignClient frontendCartFeignClient;
    /**
     * 创建订单
     * @param orderItem 订单商品项列表
     * @param tbOrder 订单
     * @param tbOrderShipping 订单收件人信息
     */
    @Override
    @LcnTransaction
    public Result insertOrder(String orderItem, TbOrder tbOrder, TbOrderShipping tbOrderShipping) {
        Date date = new Date();

        //1.利用Redis生成OrderId
        Long orderId = this.commonRedisOrderFeignClient.selectOrderId();

        // 2..插入tbOrder
        tbOrder.setOrderId(orderId+"");
        //状态：1、未付款，2、已付款，3、未发货，4、已发货，5、交易成功，6、交易关闭
        tbOrder.setStatus(1);
        tbOrder.setCreateTime(date);
        tbOrder.setUpdateTime(date);
        //0:未评价 1：已评价
        tbOrder.setBuyerRate(0);
        //将订单插入到数据库中
        this.tbOrderMapper.insert(tbOrder);

        // 3..插入tbOrderShipping
        tbOrderShipping.setOrderId(orderId.toString());
        tbOrderShipping.setUpdated(date);
        tbOrderShipping.setCreated(date);
        this.tbOrderShippingMapper.insert(tbOrderShipping);

        // 4.插入tbOrderItem
        List<TbOrderItem> orders = JsonUtils.jsonToList(orderItem, TbOrderItem.class);
        for (TbOrderItem tbOrderItem :orders) {
            tbOrderItem.setId(IDUtils.genItemId()+"");
            tbOrderItem.setOrderId(orderId+"");
            this.tbOrderItemMapper.insert(tbOrderItem);

            // 5.将订单中的商品从购物车中删除
            this.frontendCartFeignClient.deleteItemFromCart(Long.parseLong(tbOrderItem.getItemId()),
                    tbOrder.getUserId().toString());
        }

        return Result.ok(orderId);
    }
}
