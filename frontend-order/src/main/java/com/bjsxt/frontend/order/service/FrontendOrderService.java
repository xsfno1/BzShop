package com.bjsxt.frontend.order.service;

import com.bjsxt.entity.Result;
import com.bjsxt.pojo.TbOrder;
import com.bjsxt.pojo.TbOrderShipping;

public interface FrontendOrderService {
    Result insertOrder(String orderItem, TbOrder tbOrder , TbOrderShipping tbOrderShipping);
}
