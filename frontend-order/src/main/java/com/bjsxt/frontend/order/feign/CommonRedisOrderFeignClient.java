package com.bjsxt.frontend.order.feign;

import com.bjsxt.feign.redis.CommonRedisOrderFeignService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("common-redis")
public interface CommonRedisOrderFeignClient extends CommonRedisOrderFeignService {
}
