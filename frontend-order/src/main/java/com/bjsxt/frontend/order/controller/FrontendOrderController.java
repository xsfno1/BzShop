package com.bjsxt.frontend.order.controller;

import com.bjsxt.entity.Result;
import com.bjsxt.frontend.order.service.FrontendOrderService;
import com.bjsxt.pojo.TbOrder;
import com.bjsxt.pojo.TbOrderShipping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/order",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class FrontendOrderController {
    @Autowired
    private FrontendOrderService frontendOrderService;

    /**
     * 创建订单
     * @param orderItem 订单商品项列表
     * @param tbOrder 订单
     * @param tbOrderShipping 订单收件人信息
     */
    @PostMapping("/insertOrder")
    public Result insertOrder(String orderItem, TbOrder tbOrder , TbOrderShipping tbOrderShipping){
        try {
            return this.frontendOrderService.insertOrder(orderItem,tbOrder,tbOrderShipping);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.build(500,"error");
    }
}
