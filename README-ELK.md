ELK分布式日志管理

1.安装ELK（安装步骤见有道云笔记）

2.使用方法
    * 步骤1：修改 POM 文件， 添加相关Logback依赖
            <!--Logback-->
            <dependency>
                <groupId>net.logstash.logback</groupId>
                <artifactId>logstash-logback-encoder</artifactId>
            </dependency>
    * 步骤2：添加 Logback 配置文件 logback.xml
    * 步骤3：修改代码，在异常出输出日志(参考BackendItemController)
            private Logger logger = LoggerFactory.getLogger(BackendItemController.class);
            this.logger.error(e.getMessage());

3.通过 Kibana 查询日志
    http://192.168.255.153:5601/app/kibana