package com.bjsxt.feign.redis;

import com.bjsxt.entity.CartItem;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@RequestMapping(value = "/service/redis/cart", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface CommonRedisCartFeignService {
    @PostMapping("selectCartByUserId")
    Map<String, CartItem> selectCartByUserId(@RequestParam("userId") String userId);
    @PostMapping(value="insertCart",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    Integer insertCart(@RequestBody Map<String,Object> map);
}
