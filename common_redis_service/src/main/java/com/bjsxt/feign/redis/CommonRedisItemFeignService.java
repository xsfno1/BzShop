package com.bjsxt.feign.redis;

import com.bjsxt.pojo.TbItem;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping(value = "/service/redis/item",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface CommonRedisItemFeignService {
    @PostMapping(value="/insertItemBasicInfo" , consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    Integer insertItemBasicInfo(@RequestBody TbItem tbItem);
    @PostMapping(value="/selectItemBasicInfo")
    TbItem selectItemBasicInfo(@RequestParam("tbItemId") Long tbItemId);
}
