package com.bjsxt.feign.redis;

import com.bjsxt.entity.CatResult;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/service/redis/itemCategory",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface CommonRedisItemCateFeignService {
    @PostMapping(value="/insertItemCategory" , consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    Integer insertItemCategory(@RequestBody CatResult catResult);
    @GetMapping(value = "/selectItemCategory")
    CatResult selectItemCategory();
}
