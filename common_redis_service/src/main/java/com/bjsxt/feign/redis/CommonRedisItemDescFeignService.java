package com.bjsxt.feign.redis;

import com.bjsxt.pojo.TbItemDesc;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping(value = "/service/redis/itemDesc",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface CommonRedisItemDescFeignService {
    @PostMapping(value = "/insertItemDesc" , consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    Integer insertItemDesc(@RequestBody TbItemDesc tbItemDesc);
    @PostMapping(value = "/selectItemDesc")
    TbItemDesc selectItemDesc(@RequestParam("tbItemId") Long tbItemId);
}
