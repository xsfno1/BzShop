package com.bjsxt.feign.redis;

import com.bjsxt.pojo.TbItem;
import com.bjsxt.pojo.TbItemParamItem;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping(value = "/service/redis/itemParamItem",produces = MediaType.APPLICATION_JSON_UTF8_VALUE , consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface CommonRedisItemParamItemFeignService {
    @PostMapping("/insertItemParamItem")
    Integer insertItemParamItem(@RequestBody TbItemParamItem tbItemParamItem);
    @PostMapping("/selectItemParamItem")
    TbItemParamItem selectItemParamItem(@RequestParam("tbItemId") Long tbItemId);
}
