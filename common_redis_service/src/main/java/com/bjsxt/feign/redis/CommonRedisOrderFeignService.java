package com.bjsxt.feign.redis;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/service/redis/order",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface CommonRedisOrderFeignService {
    @RequestMapping("/selectOrderId")
    Long selectOrderId();
}
