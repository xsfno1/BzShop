package com.bjsxt.feign.redis;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;

@RequestMapping(value = "/service/redis/content",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface CommonRedisContentFeignService {
    @PostMapping(value="/insertContentAD" , consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    Integer insertContentAD(@RequestBody List<Map> maps);
    @GetMapping(value = "/selectContentAD")
    List<Map> selectContentAD();
}
