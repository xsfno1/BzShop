package com.bjsxt.feign.redis;

import com.bjsxt.pojo.TbUser;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RequestMapping(value = "/service/redis/user", produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface CommonRedisUserFeignService {
    @PostMapping("addUserToRedis")
    Integer addUserToRedis(@RequestBody TbUser tbUser , @RequestParam("token") String token);

    @GetMapping("selectUserFromRedis")
    TbUser selectUserFromRedis(@RequestParam("token") String token);

    @GetMapping("deleteUserFromRedis")
    Integer deleteUserFromRedis(@RequestParam("token") String token);
}
