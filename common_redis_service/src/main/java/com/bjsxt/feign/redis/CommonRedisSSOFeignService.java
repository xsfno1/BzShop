package com.bjsxt.feign.redis;

import com.bjsxt.pojo.TbUser;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping(value = "/service/redis/sso")
public interface CommonRedisSSOFeignService {
    @RequestMapping("/checkUserToken")
    TbUser checkUserToken(@RequestParam("userToken") String userToken);
}
