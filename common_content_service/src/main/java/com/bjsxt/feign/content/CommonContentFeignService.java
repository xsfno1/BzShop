package com.bjsxt.feign.content;

import com.bjsxt.entity.PageResult;
import com.bjsxt.pojo.TbContent;
import com.bjsxt.pojo.TbContentCategory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RequestMapping(value = "/service/content", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface CommonContentFeignService {
    @PostMapping("/selectContentCategoryByParentId")
    List<TbContentCategory> selectContentCategoryByParentId(@RequestParam("pid") Long pid);

    @PostMapping("/insertContentCategory")
    Integer insertContentCategory(@RequestBody TbContentCategory tbContentCategory);

    @PostMapping("/updateContentCategory")
    Integer updateContentCategory(@RequestBody TbContentCategory tbContentCategory);

    @GetMapping("/selectContentCategoryById")
    TbContentCategory selectContentCategoryById(@RequestParam("id") Long id);

    @PostMapping("/deleteContentCategoryById")
    Integer deleteContentCategoryById(@RequestParam("categoryId") Long categoryId);

    @PostMapping("/selectTbContentAllByCategoryId")
    PageResult selectTbContentAllByCategoryId(@RequestParam("categoryId") Long categoryId,
                                              @RequestParam("page") Integer page,
                                              @RequestParam("rows") Integer rows);

    @PostMapping(value = "/insertTbContent",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    Integer insertTbContent(@RequestBody TbContent tbContent);

    @PostMapping(value = "/deleteContentByIds")
    Integer deleteContentByIds(@RequestParam("id") Long id);

    @GetMapping(value = "/selectFrontendContentByAD")
    List<Map> selectFrontendContentByAD();

}
