package com.bjsxt.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

/**
 * 完成图片的上传，通过ftp将图片上传到图片服务器
 * @author xsf
 */
public class FtpUtil {
    public static void main(String[] args) throws Exception{
        String hostname = "192.168.255.140";
        int port = 21;
        String username = "ftpuser";
        String password = "ftpuser";
        String basePath = "/home/ftpuser/bz_store";
        String filePath = "";
        String filename = "gem111.jpg";
        InputStream local = null;
        try {
            local = new FileInputStream(
                    "C:\\Users\\Administrator\\Pictures\\source\\images\\gem.jpg");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        uploadImg(hostname, port, username, password, basePath,filePath, filename, local);
    }

    public static boolean uploadImg(String host, int port, String username, String password, String basePath,
                                    String filePath, String filename, InputStream input) {
        boolean flag = false;
        try {
            // 创建FtpClient对象
            FTPClient client = new FTPClient();
            // 建立和ftp服务器的链接
            client.connect(host, port);
            // 登陆ftp服务器
            client.login(username, password);
            // 设置上传的文件的类型
            client.setFileType(FTP.BINARY_FILE_TYPE);
            // 切换工作目录，文件上传后保存到那个目录
            if (!client.changeWorkingDirectory(basePath+filePath)) {
                if (client.makeDirectory(basePath+filePath)) {
                    client.changeWorkingDirectory(basePath+filePath);
                }
            }
            //这是个大坑 : 没有这语句传输的文件为空,这语句位置也很关键
            client.enterLocalPassiveMode();
            // 实现文件上传
            client.storeFile(filename, input);

            input.close();
            client.logout();
            client.disconnect();

            flag = true;
            System.out.println("=====上传成功=====");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }
}

