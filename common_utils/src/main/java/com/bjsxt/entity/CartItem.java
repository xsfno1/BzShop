package com.bjsxt.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 购物车商品模型
 */
@Data
public class CartItem implements Serializable {
    private Long id;
    private String title;
    private String image;
    private int num;
    private Long price;
    private String sellPoint;
}
