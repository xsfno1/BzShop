package com.bjsxt.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * Solr 返回给前端页面的数据模型
 */
@Data
public class SolrDocumentResult implements Serializable {
    private String id;
    private String item_title;
    private String item_sell_point;
    private String item_price;
    private String item_image;
    private String item_category_name;
    private String item_desc;
}
