package com.bjsxt.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 登录用户模型
 */
@Data
public class LoginResult implements Serializable {
    private String token;
    private String userid;
    private String username;
}
