package com.bjsxt.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class CatResult implements Serializable {
    private List<?> data;
}
