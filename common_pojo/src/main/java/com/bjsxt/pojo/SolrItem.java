package com.bjsxt.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * Solr 实体
 */
@Data
public class SolrItem implements Serializable {
    private Long id;
    private String title;
    private String sell_point;
    private Long price;
    private String image;
    private String name;
    private String item_desc;
}
