Zuul 服务网关

 * 服务网关 Zuul
 *		步骤1.修改POM文件,添加Zuul坐标依赖
 *		步骤2.修改全局配置文件
 *		步骤3.修改启动类,添加@EnableZuulProxy注释,开启Zuul网关代理

1.网关中实现对服务降级处理
     * 		步骤1.创建Fallback类,实现FallbackProvider接口,注意要添加@Component注释交以给Spring容器管理

2.网关实现限流方式一：
     * 实现网关限流,在高并发情况下，网关实现限流达到自我保护
     * 		步骤1.修改POM文件,添加zuul-ratelimit坐标
     * 		步骤2.修改全局配置文件,添加对zuul-ratelimit的配置(全局配置,局部配置)
     * 		步骤3.修改POM文件,添加Redis坐标
     * 		步骤4.修改全局配置文件,添加对Redis的配置，将限流信息存储在Redis中

3.网关实现限流方式二：利用谷歌令牌桶算法实现限流
     * 实现网关限流,在高并发情况下，网关实现限流达到自我保护
     * 		步骤1.创建限流器类 Filter

4.Zuul 统一异常处理
     * 		步骤1.创建ExceptionHandler类
    参考文章：https://www.jb51.net/article/138773.htm
