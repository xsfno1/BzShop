package com.bjsxt.common.zuul.handle;

import com.bjsxt.entity.Result;
import com.bjsxt.utils.JsonUtils;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 处理异常响应的控制器
 *
 * （暂时关闭使用）
 */
//@RestController
public class ExceptionHandler implements ErrorController {
    @Override
    public String getErrorPath() {
        return "/error";
    }
    @RequestMapping(value = "/error" , produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String error(HttpServletRequest request){
        Result result = null;
        try {
            //SpringCloud提供的 SendErrorFilter 已经将相关的错误信息放到 request 作用域当中了
            Integer code = (Integer) request.getAttribute("javax.servlet.error.status_code");
            Object exception = request.getAttribute("javax.servlet.error.exception");

            result = new Result();
            result.setStatus(code);
            result.setMsg(exception.toString());

            return JsonUtils.objectToJson(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
