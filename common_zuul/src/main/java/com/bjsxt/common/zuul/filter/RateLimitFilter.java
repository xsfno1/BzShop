package com.bjsxt.common.zuul.filter;

import com.google.common.util.concurrent.RateLimiter;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;
/**
 * 限流器
 * 网关实现限流方式二：利用谷歌令牌桶算法实现限流
 */
@Component
public class RateLimitFilter extends ZuulFilter {
    //创建令牌桶
    //RateLimiter.create(1)  1:是每秒生成令牌的数量
    //数值越大代表处理请求量月多， 数值越小代表处理请求量越少
    private static final RateLimiter RATE_LIMIT = RateLimiter.create(1);

    @Override
    public String filterType() {
        // pre :　执行前过滤
        return FilterConstants.PRE_TYPE;
    }
    /**
     * 限流器的优先级应为最高
     */
    @Override
    public int filterOrder() {
        return FilterConstants.SERVLET_DETECTION_FILTER_ORDER - 1;
    }

    /**
     * 开启过滤器 ： 默认false
     */
    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        // 1.是否能从令牌桶中获取到令牌
        if ( ! this.RATE_LIMIT.tryAcquire()) {
            // 令牌桶中没有令牌
            RequestContext requestContext = RequestContext.getCurrentContext();
            requestContext.setSendZuulResponse(false);
            // 429 : Too Many Request
            requestContext.setResponseStatusCode(429);
            System.out.println("#######################  网关限流执行  #######################");
        }
        return null;
    }
}
