package com.bjsxt.common.zuul.fallback;

import com.bjsxt.entity.Result;
import com.bjsxt.utils.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.netflix.zuul.filters.route.FallbackProvider;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * 前台首页服务 : 网关中实现对服务降级处理
 */
@Component
public class FrontendPortalFallback implements FallbackProvider {
    private final Logger logger = LoggerFactory.getLogger(FrontendPortalFallback.class);

    /**
     * getRoute方法的返回值就是要监听的挂掉的微服务名字
     * api服务id，如果需要所有调用都支持回退，则return "*"或return null
     */
    @Override
    public String getRoute() {
        return "frontend-portal";
    }
    /**
     * 当服务无法执行时，该方法返回托底信息
     */
    @Override
    public ClientHttpResponse fallbackResponse(String route, Throwable cause) {
        logger.info("--> route:{} 进行熔断降级", route);
        return new ClientHttpResponse() {
            /**
             * ClientHttpResponse的fallback的状态码
             */
            @Override
            public HttpStatus getStatusCode() throws IOException {
                return HttpStatus.OK;
            }

            @Override
            public int getRawStatusCode() throws IOException {
                return this.getStatusCode().value();
            }

            @Override
            public String getStatusText() throws IOException {
                return this.getStatusCode().getReasonPhrase();
            }

            @Override
            public void close() {

            }
            /**
             * 设置响应体信息
             */
            @Override
            public InputStream getBody() throws IOException {
                String content = JsonUtils.objectToJson(Result.error("前台首页服务超时， 请重试"));
                return new ByteArrayInputStream(content.getBytes(content));
            }
            /**
             * 设置响应头信息
             */
            @Override
            public HttpHeaders getHeaders() {
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
                return headers;
            }
        };
    }
}
