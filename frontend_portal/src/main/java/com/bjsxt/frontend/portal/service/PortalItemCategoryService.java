package com.bjsxt.frontend.portal.service;

import com.bjsxt.entity.Result;

public interface PortalItemCategoryService {
    Result selectItemCategoryAll();
}
