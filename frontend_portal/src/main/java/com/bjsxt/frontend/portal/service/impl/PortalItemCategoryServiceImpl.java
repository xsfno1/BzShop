package com.bjsxt.frontend.portal.service.impl;

import com.bjsxt.entity.CatResult;
import com.bjsxt.entity.Result;
import com.bjsxt.frontend.portal.feign.CommonItemCateFeignClient;
import com.bjsxt.frontend.portal.feign.CommonRedisItemCateFeignClient;
import com.bjsxt.frontend.portal.service.PortalItemCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PortalItemCategoryServiceImpl implements PortalItemCategoryService {
    @Autowired
    private CommonItemCateFeignClient commonItemCateFeignClient;
    @Autowired
    private CommonRedisItemCateFeignClient commonRedisItemCateFeignClient;

    /**
     * 首页左侧商品分类接口
     * @return Result
     */
    @Override
    public Result selectItemCategoryAll() {
        //查询Redis缓存
        try {
            CatResult redisCatResult = this.commonRedisItemCateFeignClient.selectItemCategory();
            if (redisCatResult != null && redisCatResult.getData().size() > 0) {
                System.out.println("##########    测试Redis缓存-首页分类 : 发现缓存！已向Redis中提取数据！   ############");
                return Result.ok(redisCatResult);
            }
            System.out.println("##########    测试Redis缓存-首页分类 : 没有发现缓存！向数据库发起查询！   ##############");
        } catch (Exception e) {
            e.printStackTrace();
        }
        //查询数据库
        CatResult catResult = this.commonItemCateFeignClient.selectItemCategoryAll();
        if (catResult != null && catResult.getData().size() > 0) {
            //添加Redis缓存
            try {
                this.commonRedisItemCateFeignClient.insertItemCategory(catResult);
                System.out.println("##########    测试Redis缓存-首页分类 : 添加Redis缓存成功！   #################");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return Result.ok(catResult);
        }
        return Result.error("查无数据");
    }
}
