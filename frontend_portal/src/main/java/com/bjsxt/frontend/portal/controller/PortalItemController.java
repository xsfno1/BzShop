package com.bjsxt.frontend.portal.controller;

import com.bjsxt.entity.Result;
import com.bjsxt.frontend.portal.service.PortalItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/frontend/item",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class PortalItemController {
    @Autowired
    private PortalItemService portalItemService;

    /**
     * 通过主键获取商品信息
     * @param itemId 主键
     */
    @PostMapping("/selectItemInfo")
    public Result selectItemInfo(Long itemId){
        try {
            return this.portalItemService.selectItemInfo(itemId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.error("error");
    }
    /**
     * 通过商品主键获取商品详情信息
     * @param itemId 商品主键
     */
    @PostMapping("/selectItemDescByItemId")
    public Result selectItemDescByItemId(Long itemId){
        try {
            return this.portalItemService.selectItemDescByItemId(itemId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.error("error");
    }
    /**
     * 通过商品主键获取商品规格参数信息
     * @param itemId 商品主键
     */
    @PostMapping("/selectTbItemParamItemByItemId")
    public Result selectTbItemParamItemByItemId(Long itemId){
        try {
            return this.portalItemService.selectTbItemParamItemByItemId(itemId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.error("error");
    }

}
