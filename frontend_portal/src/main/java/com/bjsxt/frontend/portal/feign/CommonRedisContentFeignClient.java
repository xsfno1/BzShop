package com.bjsxt.frontend.portal.feign;

import com.bjsxt.feign.redis.CommonRedisContentFeignService;
import com.bjsxt.frontend.portal.fallback.CommonRedisContentFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "common-redis",fallbackFactory = CommonRedisContentFallbackFactory.class)
public interface CommonRedisContentFeignClient extends CommonRedisContentFeignService {
}
