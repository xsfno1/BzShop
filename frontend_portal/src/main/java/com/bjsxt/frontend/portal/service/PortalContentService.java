package com.bjsxt.frontend.portal.service;

import com.bjsxt.entity.Result;

public interface PortalContentService {
    Result selectFrontendContentByAD();
}
