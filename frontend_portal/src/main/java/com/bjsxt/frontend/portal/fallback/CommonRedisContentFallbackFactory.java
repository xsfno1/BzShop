package com.bjsxt.frontend.portal.fallback;

import com.bjsxt.frontend.portal.feign.CommonRedisContentFeignClient;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class CommonRedisContentFallbackFactory implements FallbackFactory<CommonRedisContentFeignClient> {
    @Override
    public CommonRedisContentFeignClient create(Throwable throwable) {
        return new CommonRedisContentFeignClient(){
            @Override
            public Integer insertContentAD(List<Map> maps) {
                return null;
            }

            @Override
            public List<Map> selectContentAD() {
                return null;
            }
        };
    }
}
