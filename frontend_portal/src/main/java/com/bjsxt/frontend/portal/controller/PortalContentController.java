package com.bjsxt.frontend.portal.controller;

import com.bjsxt.entity.Result;
import com.bjsxt.frontend.portal.service.PortalContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/frontend/content", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class PortalContentController {
    @Autowired
    private PortalContentService portalContentService;

    /**
     * 首页大广告位接口
     * @return Result
     */
    @GetMapping("/selectFrontendContentByAD")
    public Result selectFrontendContentByAD() {
        try {
            return this.portalContentService.selectFrontendContentByAD();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.error("error");
    }
}
