package com.bjsxt.frontend.portal.service;

import com.bjsxt.entity.Result;

public interface PortalItemService {
    Result selectItemInfo(Long itemId);

    Result selectItemDescByItemId(Long itemId);

    Result selectTbItemParamItemByItemId(Long itemId);
}
