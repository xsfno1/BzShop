package com.bjsxt.frontend.portal.service.impl;

import com.bjsxt.entity.Result;
import com.bjsxt.frontend.portal.feign.CommonContentFeignClient;
import com.bjsxt.frontend.portal.feign.CommonRedisContentFeignClient;
import com.bjsxt.frontend.portal.service.PortalContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class PortalContentServiceImpl implements PortalContentService {
    @Autowired
    private CommonContentFeignClient commonContentFeignClient;
    @Autowired
    private CommonRedisContentFeignClient commonRedisContentFeignClient;
    /**
     * 首页大广告位接口
     * @return Result
     */
    @Override
    public Result selectFrontendContentByAD() {
        //查询Redis缓存
        try {
            List<Map> redisMaps = this.commonRedisContentFeignClient.selectContentAD();
            if(redisMaps!=null && redisMaps.size()>0){
                System.out.println("##########    测试Redis缓存-大广告 : 发现缓存！已向Redis中提取数据！   ############");
                return Result.ok(redisMaps);
            }
            System.out.println("##########    测试Redis缓存-大广告 : 没有发现缓存！向数据库发起查询！   ##############");
        }catch (Exception e){
            e.printStackTrace();
        }
        //查询数据库
        List<Map> maps = this.commonContentFeignClient.selectFrontendContentByAD();
        if(maps!=null && maps.size()>0){
            //添加Redis缓存
            this.commonRedisContentFeignClient.insertContentAD(maps);
            System.out.println("##########    测试Redis缓存-大广告 : 添加Redis缓存成功！   #################");
            return Result.ok(maps);
        }
        return Result.error("查无记录");
    }
}
