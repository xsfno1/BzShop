package com.bjsxt.frontend.portal.feign;

import com.bjsxt.feign.redis.CommonRedisItemParamItemFeignService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("common-redis")
public interface CommonRedisItemParamItemFeignClient extends CommonRedisItemParamItemFeignService {
}
