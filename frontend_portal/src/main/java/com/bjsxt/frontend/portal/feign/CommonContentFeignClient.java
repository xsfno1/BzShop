package com.bjsxt.frontend.portal.feign;

import com.bjsxt.feign.content.CommonContentFeignService;
import com.bjsxt.frontend.portal.fallback.CommonContentFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "common-content",fallbackFactory = CommonContentFallbackFactory.class)
public interface CommonContentFeignClient extends CommonContentFeignService {
}
