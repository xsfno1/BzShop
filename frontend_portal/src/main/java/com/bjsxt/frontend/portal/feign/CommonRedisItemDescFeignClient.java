package com.bjsxt.frontend.portal.feign;

import com.bjsxt.feign.redis.CommonRedisItemDescFeignService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("common-redis")
public interface CommonRedisItemDescFeignClient extends CommonRedisItemDescFeignService {
}
