package com.bjsxt.frontend.portal.feign;

import com.bjsxt.feign.redis.CommonRedisItemFeignService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("common-redis")
public interface CommonRedisItemFeignClient extends CommonRedisItemFeignService {
}
