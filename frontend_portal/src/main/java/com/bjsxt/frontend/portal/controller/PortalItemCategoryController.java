package com.bjsxt.frontend.portal.controller;

import com.bjsxt.entity.Result;
import com.bjsxt.frontend.portal.service.PortalItemCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/frontend/itemCategory", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class PortalItemCategoryController {
    @Autowired
    private PortalItemCategoryService portalItemCategoryService;

    /**
     * 首页左侧商品分类接口
     * @return Result
     */
    @GetMapping("selectItemCategoryAll")
    public Result selectItemCategoryAll() {
        try {
            return this.portalItemCategoryService.selectItemCategoryAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.error("error");
    }
}