package com.bjsxt.frontend.portal.service.impl;

import com.bjsxt.entity.Result;
import com.bjsxt.frontend.portal.feign.CommonItemFeignClient;
import com.bjsxt.frontend.portal.feign.CommonRedisItemDescFeignClient;
import com.bjsxt.frontend.portal.feign.CommonRedisItemFeignClient;
import com.bjsxt.frontend.portal.feign.CommonRedisItemParamItemFeignClient;
import com.bjsxt.frontend.portal.service.PortalItemService;
import com.bjsxt.pojo.TbItem;
import com.bjsxt.pojo.TbItemDesc;
import com.bjsxt.pojo.TbItemParamItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PortalItemServiceImpl implements PortalItemService {
    @Autowired
    private CommonItemFeignClient commonItemFeignClient;
    @Autowired
    private CommonRedisItemFeignClient commonRedisItemFeignClient;
    @Autowired
    private CommonRedisItemDescFeignClient commonRedisItemDescFeignClient;
    @Autowired
    private CommonRedisItemParamItemFeignClient commonRedisItemParamItemFeignClient;
    @Override
    public Result selectItemInfo(Long itemId) {
        //查询Redis缓存
        try {
            TbItem redisTbItem = this.commonRedisItemFeignClient.selectItemBasicInfo(itemId);
            if(redisTbItem != null){
                System.out.println("##########    测试Redis缓存-商品 <"+itemId+"> 信息查询 : 发现缓存！已向Redis中提取数据！   ############");
                return Result.ok(redisTbItem);
            }
            System.out.println("##########    测试Redis缓存-商品 <"+itemId+"> 信息查询 : 没有发现缓存！向数据库发起查询！   ############");
        } catch (Exception e) {
            e.printStackTrace();
        }
        //查询MySQL数据库
        TbItem tbItem = this.commonItemFeignClient.selectItemInfo(itemId);
        if(tbItem!=null){
            //添加Redis缓存
            try {
                this.commonRedisItemFeignClient.insertItemBasicInfo(tbItem);
                System.out.println("##########    测试Redis缓存-商品 <"+itemId+"> 信息查询 : 添加Redis缓存成功！   ############");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return Result.ok(tbItem);
        }
        return Result.error("查无结果");
    }
    /**
     * 通过商品主键获取商品详情信息
     * @param itemId 商品主键
     */
    @Override
    public Result selectItemDescByItemId(Long itemId) {
        //查询Redis缓存
        try {
            TbItemDesc redisTbItemDesc = this.commonRedisItemDescFeignClient.selectItemDesc(itemId);
            if(redisTbItemDesc!=null){
                System.out.println("##########    测试Redis缓存-商品 <"+itemId+"> 详情查询 : 发现缓存！已向Redis中提取数据！   ############");
                return Result.ok(redisTbItemDesc);
            }
            System.out.println("##########    测试Redis缓存-商品 <"+itemId+"> 详情查询 : 没有发现缓存！向数据库发起查询！   ############");
        } catch (Exception e) {
            e.printStackTrace();
        }
        //查询MySQL数据库
        TbItemDesc tbItemDesc = this.commonItemFeignClient.selectItemDescByItemId(itemId);
        if(tbItemDesc!=null){
            //添加Redis缓存
            try {
                this.commonRedisItemDescFeignClient.insertItemDesc(tbItemDesc);
                System.out.println("##########    测试Redis缓存-商品 <"+itemId+"> 详情查询 : 添加Redis缓存成功！   ############");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return Result.ok(tbItemDesc);
        }
        return Result.error("查无结果");
    }
    /**
     * 通过商品主键获取商品规格参数信息
     * @param itemId 商品主键
     */
    @Override
    public Result selectTbItemParamItemByItemId(Long itemId) {
        //查询Redis缓存
        try {
            TbItemParamItem redisTbItemParamItem = this.commonRedisItemParamItemFeignClient.selectItemParamItem(itemId);
            if(redisTbItemParamItem!=null){
                System.out.println("##########    测试Redis缓存-商品 <"+itemId+"> 规格参数查询 : 发现缓存！已向Redis中提取数据！   " +
                        "############");
                return Result.ok(redisTbItemParamItem);
            }
            System.out.println("##########    测试Redis缓存-商品 <"+itemId+"> 规格参数查询 : 没有发现缓存！向数据库发起查询！   ############");
        } catch (Exception e) {
            e.printStackTrace();
        }
        //查询MySQL数据库
        TbItemParamItem tbItemParamItem = this.commonItemFeignClient.selectTbItemParamItemByItemId(itemId);
        if(tbItemParamItem!=null){
            //添加Redis缓存
            try {
                this.commonRedisItemParamItemFeignClient.insertItemParamItem(tbItemParamItem);
                System.out.println("##########    测试Redis缓存-商品 <"+itemId+"> 规格参数查询 : 添加Redis缓存成功！   ############");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return Result.ok(tbItemParamItem);
        }
        return Result.error("查无结果");
    }
}
