package com.bjsxt.frontend.portal.feign;

import com.bjsxt.feign.CommonItemCatFeignService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("common-item")
public interface CommonItemCateFeignClient extends CommonItemCatFeignService {
}
