package com.bjsxt.frontend.portal.feign;

import com.bjsxt.feign.redis.CommonRedisItemCateFeignService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("common-redis")
public interface CommonRedisItemCateFeignClient extends CommonRedisItemCateFeignService {
}
