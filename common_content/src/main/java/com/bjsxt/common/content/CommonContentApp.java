package com.bjsxt.common.content;

import com.codingapi.txlcn.tc.config.EnableDistributedTransaction;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@MapperScan("com.bjsxt.mapper")
@EnableDiscoveryClient
@EnableDistributedTransaction //开启Tx事务
public class CommonContentApp {
    public static void main(String[] args) {
        SpringApplication.run(CommonContentApp.class,args);
    }
}
