package com.bjsxt.common.content.service.impl;

import com.bjsxt.common.content.service.CommonContentService;
import com.bjsxt.entity.PageResult;
import com.bjsxt.mapper.TbContentCategoryMapper;
import com.bjsxt.mapper.TbContentMapper;
import com.bjsxt.pojo.TbContent;
import com.bjsxt.pojo.TbContentCategory;
import com.bjsxt.pojo.TbContentCategoryExample;
import com.bjsxt.pojo.TbContentExample;
import com.codingapi.txlcn.tc.annotation.LcnTransaction;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CommonContentServiceImpl implements CommonContentService {
    @Autowired
    private TbContentCategoryMapper tbContentCategoryMapper;
    @Autowired
    private TbContentMapper tbContentMapper;
    /**
     * 根据父节点 ID 查询子节点
     *
     * @param pid 父节点ID
     */
    @Override
    public List<TbContentCategory> selectContentCategoryByParentId(Long pid) {
        TbContentCategoryExample example = new TbContentCategoryExample();
        TbContentCategoryExample.Criteria criteria = example.createCriteria();
        criteria.andParentIdEqualTo(pid);
        return this.tbContentCategoryMapper.selectByExample(example);
    }

    /**
     * 内容分类添加
     *
     * @param tbContentCategory 内容分类对象
     */
    @Override
    @LcnTransaction
    public Integer insertContentCategory(TbContentCategory tbContentCategory) {
        return this.tbContentCategoryMapper.insertSelective(tbContentCategory);
    }

    /**
     * 内容分类更新
     *
     * @param tbContentCategory 内容分类对象
     */
    @Override
    @LcnTransaction
    public Integer updateContentCategory(TbContentCategory tbContentCategory) {
        // ####################    Tx-LCN 分布式事务测试  测试结果（成功）   ##################################
        // int i = 1/0;
        // ####################    Tx-LCN 分布式事务测试    ##############################################
        return this.tbContentCategoryMapper.updateByPrimaryKeySelective(tbContentCategory);
    }

    /**
     * 根据主键查询
     *
     * @param id 主键
     */
    @Override
    public TbContentCategory selectContentCategoryById(Long id) {
        return this.tbContentCategoryMapper.selectByPrimaryKey(id);
    }

    /**
     * 分类内容 删除  : 迭代删除子节点
     *
     * @param categoryId 主键
     */
    @Override
    @LcnTransaction
    public Integer deleteContentCategoryById(Long categoryId) {
        //查询当前节点
        TbContentCategory currentCategory = this.tbContentCategoryMapper.selectByPrimaryKey(categoryId);

        //删除当前节点的子节点
        Integer status = this.deleteNode(currentCategory);

        //查询当前节点的父节点
        TbContentCategory parentCategory =
                this.tbContentCategoryMapper.selectByPrimaryKey(currentCategory.getParentId());

        //查看当前节点是否还有子节点， 决定是否修改父节点的状态。
        TbContentCategoryExample example = new TbContentCategoryExample();
        TbContentCategoryExample.Criteria criteria = example.createCriteria();
        criteria.andParentIdEqualTo(parentCategory.getId());
        List<TbContentCategory> list = this.tbContentCategoryMapper.selectByExample(example);

        // ####################    Tx-LCN 分布式事务测试  测试结果（成功）   ##################################
        // int i = 1/0;
        // ####################    Tx-LCN 分布式事务测试    ##############################################

        if (list.size() == 0) {
            //修改父节点的状态 : isParant=0
            parentCategory.setIsParent(false);
            parentCategory.setUpdated(new Date());
            this.tbContentCategoryMapper.updateByPrimaryKey(parentCategory);
        }

        return 200;
    }

    private Integer deleteNode(TbContentCategory currentCategory) {
        if (currentCategory.getIsParent()) {
            //是父节点
            //查询当前节点所有的子节点
            TbContentCategoryExample example = new TbContentCategoryExample();
            TbContentCategoryExample.Criteria criteria = example.createCriteria();
            criteria.andParentIdEqualTo(currentCategory.getId());
            List<TbContentCategory> list = this.tbContentCategoryMapper.selectByExample(example);
            for (TbContentCategory children : list) {
                this.deleteNode(children);
                this.tbContentCategoryMapper.deleteByPrimaryKey(currentCategory.getId());
            }
        } else {
            //不是父节点
            this.tbContentCategoryMapper.deleteByPrimaryKey(currentCategory.getId());
        }
        return 200;
    }

    /**
     * 内容管理	查询接口
     * @param categoryId 内容分类主键
     * @param page       当前页
     * @param rows       页记录数
     */
    @Override
    public PageResult selectTbContentAllByCategoryId(Long categoryId, Integer page, Integer rows) {
        PageHelper.startPage(page, rows);

        TbContentExample example = new TbContentExample();
        TbContentExample.Criteria criteria = example.createCriteria();
        criteria.andCategoryIdEqualTo(categoryId);
        List<TbContent> list = this.tbContentMapper.selectByExample(example);

        PageInfo<TbContent> pageInfo = new PageInfo<>(list);

        PageResult pageResult = new PageResult();
        pageResult.setTotalPage(pageInfo.getTotal());
        pageResult.setPageIndex(page);
        pageResult.setResult(list);

        return pageResult;
    }

    /**
     * 内容管理 添加接口
     * @param tbContent 内容对象
     */
    @Override
    public Integer insertTbContent(TbContent tbContent) {
        return this.tbContentMapper.insertSelective(tbContent);
    }

    /**
     * 内容管理	删除接口
     * @param id 主键
     */
    @Override
    public Integer deleteContentByIds(Long id) {
        return this.tbContentMapper.deleteByPrimaryKey(id);
    }

    /**
     * 首页大广告位接口
     * @return List<Map>
     */
    @Override
    public List<Map> selectFrontendContentByAD() {
        List<Map> resultList = new ArrayList<>();

        TbContentExample example = new TbContentExample();
        List<TbContent> tbContents = this.tbContentMapper.selectByExample(example);
        Map<String,Object> map = null;
        for (TbContent tbContent : tbContents) {
            map = new HashMap<>();
            map.put("heightB",240);
            map.put("src",tbContent.getPic());
            map.put("width", 670);
            map.put("alt", tbContent.getSubTitle());
            map.put("srcB", null);
            map.put("widthB", 550);
            map.put("href", tbContent.getUrl());
            map.put("height", 240);

            resultList.add(map);
        }

        return resultList;
    }
}
