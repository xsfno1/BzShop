package com.bjsxt.common.content.service;

import com.bjsxt.entity.PageResult;
import com.bjsxt.pojo.TbContent;
import com.bjsxt.pojo.TbContentCategory;

import java.util.List;
import java.util.Map;

public interface CommonContentService {
    List<TbContentCategory> selectContentCategoryByParentId(Long pid);

    Integer insertContentCategory(TbContentCategory tbContentCategory);

    Integer updateContentCategory(TbContentCategory tbContentCategory);

    TbContentCategory selectContentCategoryById(Long id);

    Integer deleteContentCategoryById(Long categoryId);

    PageResult selectTbContentAllByCategoryId(Long categoryId , Integer page , Integer rows);

    Integer insertTbContent(TbContent tbContent);

    Integer deleteContentByIds(Long id);

    List<Map> selectFrontendContentByAD();
}
