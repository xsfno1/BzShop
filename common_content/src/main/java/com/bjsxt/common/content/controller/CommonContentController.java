package com.bjsxt.common.content.controller;

import com.bjsxt.common.content.service.CommonContentService;
import com.bjsxt.entity.PageResult;
import com.bjsxt.feign.content.CommonContentFeignService;
import com.bjsxt.pojo.TbContent;
import com.bjsxt.pojo.TbContentCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class CommonContentController implements CommonContentFeignService {
    @Autowired
    private CommonContentService commonContentService;

    /**
     * 根据父节点 ID 查询子节点
     *
     * @param pid 父节点ID
     */
    @Override
    public List<TbContentCategory> selectContentCategoryByParentId(Long pid) {
        return this.commonContentService.selectContentCategoryByParentId(pid);
    }

    /**
     * 内容分类添加
     *
     * @param tbContentCategory 内容分类对象
     */
    @Override
    public Integer insertContentCategory(@RequestBody TbContentCategory tbContentCategory) {
        return this.commonContentService.insertContentCategory(tbContentCategory);
    }

    /**
     * 内容分类更新
     *
     * @param tbContentCategory 内容分类对象
     */
    @Override
    public Integer updateContentCategory(@RequestBody TbContentCategory tbContentCategory) {
        return this.commonContentService.updateContentCategory(tbContentCategory);
    }

    /**
     * 根据主键查询
     *
     * @param id 主键
     */
    @Override
    public TbContentCategory selectContentCategoryById(Long id) {
        return this.commonContentService.selectContentCategoryById(id);
    }

    /**
     * 分类内容 删除
     * @param categoryId 主键
     */
    @Override
    public Integer deleteContentCategoryById(Long categoryId) {
        return this.commonContentService.deleteContentCategoryById(categoryId);
    }

    /**
     * 内容管理	查询接口
     * @param categoryId 内容分类主键
     * @param page 当前页
     * @param rows 页记录数
     */
    @Override
    public PageResult selectTbContentAllByCategoryId(@RequestParam Long categoryId, @RequestParam Integer page,
                                                     @RequestParam Integer rows) {
        return this.commonContentService.selectTbContentAllByCategoryId(categoryId,page,rows);
    }

    /**
     * 内容管理 添加接口
     * @param tbContent 内容对象
     */
    @Override
    public Integer insertTbContent(@RequestBody TbContent tbContent) {
        return this.commonContentService.insertTbContent(tbContent);
    }

    /**
     * 内容管理	删除接口
     * @param id 主键
     */
    @Override
    public Integer deleteContentByIds(@RequestParam Long id) {
        return this.commonContentService.deleteContentByIds(id);
    }

    /**
     * 首页大广告位接口
     * @return List<Map>
     */
    @Override
    public List<Map> selectFrontendContentByAD() {
        return this.commonContentService.selectFrontendContentByAD();
    }
}
