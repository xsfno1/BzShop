package com.bjsxt.feign;

import com.bjsxt.entity.PageResult;
import com.bjsxt.pojo.TbItem;
import com.bjsxt.pojo.TbItemDesc;
import com.bjsxt.pojo.TbItemParamItem;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@RequestMapping(value = "/service/item", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface CommonItemFeignService {
    @GetMapping("/selectTbItemAllByPage")
    PageResult selectTbItemAllByPage(@RequestParam("page") Integer page, @RequestParam("rows") Integer rows);

    @PostMapping("/insertTbItem")
    Integer insertTbItem(@RequestBody TbItem tbItem);

    @PostMapping("/deleteItemById")
    Integer deleteItemById(@RequestBody TbItem tbItem);

    @PostMapping("/preUpdateItem")
    Map<String, Object> preUpdateItem(@RequestParam("itemId") Long itemId);

    @PostMapping("/updateTbItem")
    Integer updateTbItem(@RequestBody TbItem tbItem);

    @PostMapping("/selectItemInfo")
    TbItem selectItemInfo(@RequestParam("itemId") Long itemId);

    @PostMapping("/selectItemDescByItemId")
    TbItemDesc selectItemDescByItemId(@RequestParam("itemId") Long itemId);

    @PostMapping("/selectTbItemParamItemByItemId")
    TbItemParamItem selectTbItemParamItemByItemId(@RequestParam("itemId") Long itemId);
}
