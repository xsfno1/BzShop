package com.bjsxt.feign;

import com.bjsxt.entity.PageResult;
import com.bjsxt.pojo.TbItemParam;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RequestMapping(value = "/service/itemParam", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface CommonItemParamFeignService {
    @GetMapping("/selectItemParamByItemCatId")
    TbItemParam selectItemParamByItemCatId(@RequestParam("itemCatId") Long itemCatId);

    @GetMapping("/selectItemParamAll")
    PageResult selectItemParamAll(@RequestParam("page") Integer page, @RequestParam("rows") Integer rows);

    @PostMapping(value = "/insertItemParam", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    Integer insertItemParam(@RequestBody TbItemParam tbItemParam);

    @GetMapping(value = "/deleteItemParamById")
    Integer deleteItemParamById(@RequestParam("id") Long id);
}
