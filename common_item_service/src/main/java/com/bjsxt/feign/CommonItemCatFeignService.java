package com.bjsxt.feign;

import com.bjsxt.entity.CatResult;
import com.bjsxt.pojo.TbItemCat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@RequestMapping(value = "/service/itemCat", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface CommonItemCatFeignService {
    @PostMapping("/selectItemCategoryByParentId")
    List<TbItemCat> selectItemCategoryByParentId(@RequestParam("parentId") Long parentId);

    @GetMapping("/selectItemCategoryAll")
    CatResult selectItemCategoryAll();
}
