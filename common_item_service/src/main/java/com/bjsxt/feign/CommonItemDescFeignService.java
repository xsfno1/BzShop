package com.bjsxt.feign;

import com.bjsxt.pojo.TbItemDesc;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/service/itemDesc", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface CommonItemDescFeignService {
    @PostMapping("/insertTbItemDesc")
    Integer insertTbItemDesc(@RequestBody TbItemDesc tbItemDesc);

    @PostMapping("/updateTbItemDesc")
    Integer updateTbItemDesc(@RequestBody TbItemDesc tbItemDesc);

}
