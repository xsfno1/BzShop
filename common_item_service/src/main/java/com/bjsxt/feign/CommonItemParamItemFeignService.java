package com.bjsxt.feign;

import com.bjsxt.pojo.TbItemParamItem;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/service/itemParamItem",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface CommonItemParamItemFeignService {
    @PostMapping("/insertTbItemParamItem")
    Integer insertTbItemParamItem(@RequestBody TbItemParamItem tbItemParamItem);
    @PostMapping("/updateTbItemParamItem")
    Integer updateTbItemParamItem(@RequestBody TbItemParamItem tbItemParamItem);
}
