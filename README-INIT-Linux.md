Linxu启动

1.启动Eureka
    192.168.255.145
    $ ./usr/local/eureka/server.sh start

2.启动Redis
    192.168.255.140
    $ cd /usr/local/redis/bin
    $ ./redis-server redis.conf

3.启动TX-Manager
    192.168.255.145
    $ cd /usr/local/bz_store/tx-lcn/txlcn-tm-5.0.2.RELEASE
    $ ./server.sh start

4.启动RabbitMQ
    192.168.255.145
    $ cd /usr/local/soft/rabbitmq_server-3.6.1/sbin
    $ ./rabbitmq-server -detached   #后台运行rabbitmq

5.启动Config-Server
    192.168.255.145
    $ cd /usr/local/bz_store
    $ ./common-config-bus-server/server.sh start