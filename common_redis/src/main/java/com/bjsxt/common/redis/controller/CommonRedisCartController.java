package com.bjsxt.common.redis.controller;

import com.bjsxt.common.redis.service.CommonRedisCartService;
import com.bjsxt.entity.CartItem;
import com.bjsxt.feign.redis.CommonRedisCartFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class CommonRedisCartController implements CommonRedisCartFeignService {
    @Autowired
    private CommonRedisCartService commonRedisCartService;
    /**
     * 根据用户 ID 查询用户购物车
     * @param userId Hash-KEY
     */
    @Override
    public Map<String, CartItem> selectCartByUserId(@RequestParam String userId) {
        return this.commonRedisCartService.selectCartByUserId(userId);
    }

    /**
     * 将购物车缓存到 redis 中
     * @param map 购物车对象
     */
    @Override
    public Integer insertCart(@RequestBody Map<String, Object> map) {
        return this.commonRedisCartService.insertCart(map);
    }

}
