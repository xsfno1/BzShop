package com.bjsxt.common.redis.service;

import java.util.List;
import java.util.Map;

public interface CommonRedisContentService {
    Integer insertContentAD(List<Map> maps);

    List<Map> selectContentAD();
}
