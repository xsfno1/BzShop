package com.bjsxt.common.redis.service.impl;

import com.bjsxt.common.redis.service.CommonRedisItemParamItemService;
import com.bjsxt.pojo.TbItemParamItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class CommonRedisItemParamItemServiceImpl implements CommonRedisItemParamItemService {
    @Autowired
    private RedisTemplate<String,Object> redisTemplate;
    @Value("${FRONTEND_ITEM_PARAM_ITEM_BASIC_REDIS_KEY}")
    private String FRONTEND_ITEM_PARAM_ITEM_BASIC_REDIS_KEY;  //Redis缓存的商品规格参数项的基本KEY
    @Value("${FRONTEND_REDIS_KEY_EXPIRE}")
    private Integer FRONTEND_REDIS_KEY_EXPIRE;  //Redis过期时间
    /**
     * 缓存商品的规格参数项
     * @param tbItemParamItem 商品规格参数项对象
     */
    @Override
    public Integer insertItemParamItem(TbItemParamItem tbItemParamItem) {
        // 补全KEY值 : BasicKey + itemId
        String key = this.FRONTEND_ITEM_PARAM_ITEM_BASIC_REDIS_KEY + ":"+tbItemParamItem.getItemId();

        this.redisTemplate.opsForValue().set(key,tbItemParamItem,this.FRONTEND_REDIS_KEY_EXPIRE, TimeUnit.SECONDS);
        return 200;
    }
    /**
     * 查询缓存中的商品规格参数
     * @param tbItemId 商品主键
     */
    @Override
    public TbItemParamItem selectItemParamItem(Long tbItemId) {
        // 补全KEY值 : BasicKey + itemId
        String key = this.FRONTEND_ITEM_PARAM_ITEM_BASIC_REDIS_KEY + ":"+tbItemId;

        return (TbItemParamItem)this.redisTemplate.opsForValue().get(key);
    }
}
