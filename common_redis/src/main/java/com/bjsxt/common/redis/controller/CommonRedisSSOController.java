package com.bjsxt.common.redis.controller;

import com.bjsxt.common.redis.service.CommonRedisSSOService;
import com.bjsxt.feign.redis.CommonRedisSSOFeignService;
import com.bjsxt.pojo.TbUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommonRedisSSOController implements CommonRedisSSOFeignService {
    @Autowired
    private CommonRedisSSOService commonRedisSSOService;

    /**
     * 根据用户 token 校验用户在 redis 中是否失效
     * @param userToken 用户令牌
     */
    @Override
    public TbUser checkUserToken(@RequestParam String userToken) {
        return this.commonRedisSSOService.checkUserToken(userToken);
    }
}
