package com.bjsxt.common.redis.controller;

import com.bjsxt.common.redis.service.CommonRedisUserService;
import com.bjsxt.feign.redis.CommonRedisUserFeignService;
import com.bjsxt.pojo.TbUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommonRedisUserController implements CommonRedisUserFeignService {
    @Autowired
    private CommonRedisUserService commonRedisUserService;

    /**
     * 缓存用户登录状态
     * @param tbUser 用户对象
     * @param token KEY
     */
    @Override
    public Integer addUserToRedis(@RequestBody TbUser tbUser , @RequestParam String token) {
        return this.commonRedisUserService.addUserToRedis(tbUser,token);
    }

    /**
     * 查询缓存用户登录状态
     * @param token KEY
     */
    @Override
    public TbUser selectUserFromRedis(@RequestParam String token) {
        return this.commonRedisUserService.selectUserFromRedis(token);
    }
    /**
     * 删除缓存用户登录状态
     * @param token KEY
     */
    @Override
    public Integer deleteUserFromRedis(@RequestParam String token) {
        return this.commonRedisUserService.deleteUserFromRedis(token);
    }
}
