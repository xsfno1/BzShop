package com.bjsxt.common.redis.service.impl;

import com.bjsxt.common.redis.service.CommonRedisCartService;
import com.bjsxt.entity.CartItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class CommonRedisCartServiceImpl implements CommonRedisCartService {
    @Autowired
    private RedisTemplate<String,Object> redisTemplate;
    @Value("${FRONTEND_CART_REDIS_KEY}")
    private String FRONTEND_CART_REDIS_KEY;
    /**
     * 根据用户 ID 查询用户购物车
     * @param userId Hash-KEY
     */
    @Override
    public Map<String, CartItem> selectCartByUserId(String userId) {
        return (Map<String, CartItem>) this.redisTemplate.opsForHash().get(this.FRONTEND_CART_REDIS_KEY,userId);
    }
    /**
     * 将购物车缓存到 redis 中
     * @param map 购物车对象
     */
    @Override
    public Integer insertCart(Map<String, Object> map) {
        String userId = (String) map.get("userId");
        Map<String,CartItem> cart = (Map<String, CartItem>) map.get("cart");
        this.redisTemplate.opsForHash().put(this.FRONTEND_CART_REDIS_KEY,userId,cart);
        return 200;
    }
}
