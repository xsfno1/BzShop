package com.bjsxt.common.redis.controller;

import com.bjsxt.common.redis.service.CommonRedisItemCateService;
import com.bjsxt.entity.CatResult;
import com.bjsxt.feign.redis.CommonRedisItemCateFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommonRedisItemCateController implements CommonRedisItemCateFeignService {
    @Autowired
    private CommonRedisItemCateService commonRedisItemCateService;

    /**
     * 添加缓存首页商品分类
     * @param catResult 首页商品分类
     */
    @Override
    public Integer insertItemCategory(@RequestBody CatResult catResult) {
        return this.commonRedisItemCateService.insertItemCategory(catResult);
    }
    /**
     * 查询缓存中的首页商品分类
     */
    @Override
    public CatResult selectItemCategory() {
        return this.commonRedisItemCateService.selectItemCategory();
    }
}
