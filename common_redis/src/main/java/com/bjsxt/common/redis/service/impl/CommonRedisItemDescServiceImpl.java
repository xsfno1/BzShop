package com.bjsxt.common.redis.service.impl;

import com.bjsxt.common.redis.service.CommonRedisItemDescService;
import com.bjsxt.pojo.TbItemDesc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class CommonRedisItemDescServiceImpl implements CommonRedisItemDescService {
    @Autowired
    private RedisTemplate<String,Object> redisTemplate;
    @Value("${FRONTEND_ITEM_DESC_BASIC_REDIS_KEY}")
    private String FRONTEND_ITEM_DESC_BASIC_REDIS_KEY;  //Redis缓存的商品详情的基本KEY
    @Value("${FRONTEND_REDIS_KEY_EXPIRE}")
    private Integer FRONTEND_REDIS_KEY_EXPIRE;  //Redis过期时间
    /**
     * 缓存商品介绍信息
     * @param tbItemDesc 商品介绍对象
     */
    @Override
    public Integer insertItemDesc(TbItemDesc tbItemDesc) {
        // 补全KEY值 : BasicKey + itemId
        String key = this.FRONTEND_ITEM_DESC_BASIC_REDIS_KEY + ":"+tbItemDesc.getItemId();

        this.redisTemplate.opsForValue().set(key,tbItemDesc,this.FRONTEND_REDIS_KEY_EXPIRE, TimeUnit.SECONDS);
        return 200;
    }
    /**
     * 查询缓存中的商品介绍
     * @param tbItemId 商品主键
     */
    @Override
    public TbItemDesc selectItemDesc(Long tbItemId) {
        // 补全KEY值 : BasicKey + itemId
        String key = this.FRONTEND_ITEM_DESC_BASIC_REDIS_KEY + ":"+tbItemId;
        return (TbItemDesc)this.redisTemplate.opsForValue().get(key);
    }
}
