package com.bjsxt.common.redis.service;

import com.bjsxt.entity.CatResult;

public interface CommonRedisItemCateService {
    Integer insertItemCategory(CatResult catResult);

    CatResult selectItemCategory();
}
