package com.bjsxt.common.redis.service.impl;

import com.bjsxt.common.redis.service.CommonRedisUserService;
import com.bjsxt.pojo.TbItem;
import com.bjsxt.pojo.TbUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class CommonRedisUserServiceImpl implements CommonRedisUserService {
    @Autowired
    private RedisTemplate<String , Object> redisTemplate;
    @Value("${FRONTEND_SSO_USER_SESSION_REDIS_KEY}")
    private String FRONTEND_SSO_USER_SESSION_REDIS_KEY;  //Redis缓存用户登录状态的基本KEY
    @Value("${FRONTEND_REDIS_KEY_EXPIRE}")
    private Integer FRONTEND_REDIS_KEY_EXPIRE;  //Redis过期时间
    /**
     * 缓存用户登录状态
     * @param tbUser 用户对象
     * @param token KEY
     */
    @Override
    public Integer addUserToRedis(TbUser tbUser,String token) {
        // 出于安全考虑，将密码置空
        tbUser.setPassword("");

        // 补全KEY值 : BasicKey + id
        String key = this.FRONTEND_SSO_USER_SESSION_REDIS_KEY + ":"+token;

        this.redisTemplate.opsForValue().set(key,tbUser,this.FRONTEND_REDIS_KEY_EXPIRE,TimeUnit.SECONDS);
        return 200;
    }
    /**
     * 查询缓存用户登录状态
     * @param token KEY
     */
    @Override
    public TbUser selectUserFromRedis(String token) {
        // 补全KEY值 : BasicKey + id
        String key = this.FRONTEND_SSO_USER_SESSION_REDIS_KEY + ":"+token;

        return (TbUser)this.redisTemplate.opsForValue().get(key);
    }
    /**
     * 删除缓存用户登录状态
     * @param token KEY
     */
    @Override
    public Integer deleteUserFromRedis(String token) {
        // 补全KEY值 : BasicKey + id
        String key = this.FRONTEND_SSO_USER_SESSION_REDIS_KEY + ":"+token;
        //删除
        this.redisTemplate.delete(key);

        return 200;
    }
}
