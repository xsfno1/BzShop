package com.bjsxt.common.redis.controller;

import com.bjsxt.common.redis.service.CommonRedisContentService;
import com.bjsxt.feign.redis.CommonRedisContentFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class CommonRedisContentController implements CommonRedisContentFeignService {
    @Autowired
    private CommonRedisContentService commonRedisContentService;

    /**
     * 将大广告位的数据添加到缓存中
     */
    @Override
    public Integer insertContentAD(@RequestBody List<Map> maps) {
        return this.commonRedisContentService.insertContentAD(maps);
    }

    /**
     * 查询缓存中首页大广告位的数据
     */
    @Override
    public List<Map> selectContentAD() {
        return this.commonRedisContentService.selectContentAD();
    }
}
