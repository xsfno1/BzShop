package com.bjsxt.common.redis.service;

import com.bjsxt.entity.CartItem;

import java.util.Map;

public interface CommonRedisCartService {
    Map<String, CartItem> selectCartByUserId(String userId);

    Integer insertCart(Map<String, Object> map);

}
