package com.bjsxt.common.redis.controller;

import com.bjsxt.common.redis.service.CommonRedisItemService;
import com.bjsxt.feign.redis.CommonRedisItemFeignService;
import com.bjsxt.pojo.TbItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommonRedisItemController implements CommonRedisItemFeignService {
    @Autowired
    private CommonRedisItemService commonRedisItemService;

    /**
     * 缓存商品信息
     * @param tbItem 商品对象
     */
    @Override
    public Integer insertItemBasicInfo(@RequestBody TbItem tbItem) {
        return this.commonRedisItemService.insertItemBasicInfo(tbItem);
    }
    /**
     * 查询缓存中的商品基本信息
     * @param tbItemId 商品主键
     */
    @Override
    public TbItem selectItemBasicInfo(@RequestParam Long tbItemId) {
        return this.commonRedisItemService.selectItemBasicInfo(tbItemId);
    }
}
