package com.bjsxt.common.redis.service;

import com.bjsxt.pojo.TbUser;

public interface CommonRedisSSOService {
    TbUser checkUserToken(String userToken);
}
