package com.bjsxt.common.redis.controller;

import com.bjsxt.common.redis.service.CommonRedisItemParamItemService;
import com.bjsxt.feign.redis.CommonRedisItemParamItemFeignService;
import com.bjsxt.pojo.TbItemParamItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommonRedisItemParamItemController implements CommonRedisItemParamItemFeignService {
    @Autowired
    private CommonRedisItemParamItemService commonRedisItemParamItemService;
    /**
     * 缓存商品的规格参数
     * @param tbItemParamItem 商品规格参数项对象
     */
    @Override
    public Integer insertItemParamItem(@RequestBody TbItemParamItem tbItemParamItem) {
        return this.commonRedisItemParamItemService.insertItemParamItem(tbItemParamItem);
    }

    /**
     * 查询缓存中的商品规格参数
     * @param tbItemId 商品主键
     */
    @Override
    public TbItemParamItem selectItemParamItem(@RequestParam Long tbItemId) {
        return this.commonRedisItemParamItemService.selectItemParamItem(tbItemId);
    }
}
