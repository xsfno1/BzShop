package com.bjsxt.common.redis.service.impl;

import com.bjsxt.common.redis.service.CommonRedisItemService;
import com.bjsxt.pojo.TbItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class CommonRedisItemServiceImpl implements CommonRedisItemService {
    @Autowired
    private RedisTemplate<String,Object> redisTemplate;
    @Value("${FRONTEND_ITEM_BASIC_INFO_REDIS_KEY}")
    private String FRONTEND_ITEM_BASIC_INFO_REDIS_KEY;  //Redis缓存的商品信息的基本KEY
    @Value("${FRONTEND_REDIS_KEY_EXPIRE}")
    private Integer FRONTEND_REDIS_KEY_EXPIRE;  //Redis过期时间

    /**
     * 缓存商品信息
     * @param tbItem 商品对象
     */
    @Override
    public Integer insertItemBasicInfo(TbItem tbItem) {
        // 补全KEY值 : BasicKey + id
        String key = this.FRONTEND_ITEM_BASIC_INFO_REDIS_KEY + ":"+tbItem.getId();

        this.redisTemplate.opsForValue().set(key,tbItem,this.FRONTEND_REDIS_KEY_EXPIRE,TimeUnit.SECONDS);
        return 200;
    }
    /**
     * 查询缓存中的商品基本信息
     * @param tbItemId 商品主键
     */
    @Override
    public TbItem selectItemBasicInfo(Long tbItemId) {
        // 补全KEY值 : BasicKey + id
        String key = this.FRONTEND_ITEM_BASIC_INFO_REDIS_KEY + ":"+tbItemId;

        return (TbItem)this.redisTemplate.opsForValue().get(key);
    }
}
