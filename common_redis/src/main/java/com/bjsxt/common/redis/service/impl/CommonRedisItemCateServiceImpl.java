package com.bjsxt.common.redis.service.impl;

import com.bjsxt.common.redis.service.CommonRedisItemCateService;
import com.bjsxt.entity.CatResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class CommonRedisItemCateServiceImpl implements CommonRedisItemCateService {
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    @Value("${FRONTEND_CATEGORY_REDIS_KEY}")
    private String FRONTEND_CATEGORY_REDIS_KEY ;       // Redis中首页商品分类的KEY值
    @Value("${FRONTEND_REDIS_KEY_EXPIRE}")
    private Integer FRONTEND_REDIS_KEY_EXPIRE ;       // Redis中过期时间
    /**
     * 向缓存中添加首页商品分类
     * @param catResult 首页商品分类对象
     */
    @Override
    public Integer insertItemCategory(CatResult catResult) {
        this.redisTemplate.opsForValue().set(this.FRONTEND_CATEGORY_REDIS_KEY,catResult,this.FRONTEND_REDIS_KEY_EXPIRE, TimeUnit.SECONDS);
        return 200;
    }
    /**
     * 查询缓存中的首页商品分类
     */
    @Override
    public CatResult selectItemCategory() {
        return (CatResult)this.redisTemplate.opsForValue().get(this.FRONTEND_CATEGORY_REDIS_KEY);
    }
}
