package com.bjsxt.common.redis.service;

import com.bjsxt.pojo.TbItemParamItem;

public interface CommonRedisItemParamItemService {
    Integer insertItemParamItem(TbItemParamItem tbItemParamItem);

    TbItemParamItem selectItemParamItem(Long tbItemId);
}
