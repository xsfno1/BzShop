package com.bjsxt.common.redis.service;

import com.bjsxt.pojo.TbUser;

public interface CommonRedisUserService {
    Integer addUserToRedis(TbUser tbUser,String token);

    TbUser selectUserFromRedis(String token);

    Integer deleteUserFromRedis(String token);
}
