package com.bjsxt.common.redis.service.impl;

import com.bjsxt.common.redis.service.CommonRedisSSOService;
import com.bjsxt.pojo.TbUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class CommonRedisSSOServiceImpl implements CommonRedisSSOService {
    @Autowired
    private RedisTemplate<String,Object> redisTemplate;
    @Value("${FRONTEND_SSO_USER_SESSION_REDIS_KEY}")
    private String FRONTEND_SSO_USER_SESSION_REDIS_KEY;

    /**
     * 根据用户 token 校验用户在 redis 中是否失效
     * @param userToken 用户令牌
     */
    @Override
    public TbUser checkUserToken(String userToken) {
        return (TbUser) this.redisTemplate.opsForValue().get(this.FRONTEND_SSO_USER_SESSION_REDIS_KEY+":"+userToken);
    }
}
