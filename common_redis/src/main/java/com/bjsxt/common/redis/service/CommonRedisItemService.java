package com.bjsxt.common.redis.service;

import com.bjsxt.pojo.TbItem;

public interface CommonRedisItemService {
    Integer insertItemBasicInfo(TbItem tbItem);

    TbItem selectItemBasicInfo(Long tbItemId);
}
