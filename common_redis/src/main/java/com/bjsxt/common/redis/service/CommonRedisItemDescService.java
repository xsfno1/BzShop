package com.bjsxt.common.redis.service;

import com.bjsxt.pojo.TbItemDesc;

public interface CommonRedisItemDescService {
    Integer insertItemDesc(TbItemDesc tbItemDesc);

    TbItemDesc selectItemDesc(Long tbItemId);
}
