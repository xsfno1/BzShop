package com.bjsxt.common.redis.service.impl;

import com.bjsxt.common.redis.service.CommonRedisContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class CommonRedisContentServiceImpl implements CommonRedisContentService {
    @Autowired
    private RedisTemplate<String , Object> redisTemplate;
    @Value("${FRONTEND_AD_REDIS_KEY}")
    private String FRONTEND_AD_REDIS_KEY;
    @Value("${FRONTEND_REDIS_KEY_EXPIRE}")
    private Integer FRONTEND_REDIS_KEY_EXPIRE;
    /**
     * 将大广告位的数据添加到缓存中
     */
    @Override
    public Integer insertContentAD(List<Map> maps) {
        this.redisTemplate.opsForValue().set(this.FRONTEND_AD_REDIS_KEY,maps,this.FRONTEND_REDIS_KEY_EXPIRE, TimeUnit.SECONDS);
        return 200;
    }
    /**
     * 查询缓存中首页大广告位的数据
     */
    @Override
    public List<Map> selectContentAD() {
        return (List<Map>) this.redisTemplate.opsForValue().get(this.FRONTEND_AD_REDIS_KEY);
    }
}
