package com.bjsxt.common.redis.service.impl;

import com.bjsxt.common.redis.service.CommonRedisOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class CommonRedisOrderServiceImpl implements CommonRedisOrderService {
    @Value("${order_item_id_key}")
    private String order_item_id_key;
    @Value("${init_item_id}")
    private Long init_item_id;
    @Autowired
    private RedisTemplate<String,Object> redisTemplate;
    /**
     * 生成订单 ID
     */
    @Override
    public Long selectOrderId() {
        // 1.判断是否Redis中存在订单ID
        Integer key = (Integer) this.redisTemplate.opsForValue().get(this.order_item_id_key);
        // 2.如果订单 ID 的值不存在，将默认值 set 到 redis 中
        if(key == null || key <=0){
            this.redisTemplate.opsForValue().set(this.order_item_id_key,this.init_item_id);
        }
        // 3.通过 redis 中的自增长将默认值做递增处理
        Long orderId = this.redisTemplate.opsForValue().increment(this.order_item_id_key);
        return orderId;
    }
}
