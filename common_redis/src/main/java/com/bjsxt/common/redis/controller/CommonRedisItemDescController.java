package com.bjsxt.common.redis.controller;

import com.bjsxt.common.redis.service.CommonRedisItemDescService;
import com.bjsxt.feign.redis.CommonRedisItemDescFeignService;
import com.bjsxt.pojo.TbItemDesc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommonRedisItemDescController implements CommonRedisItemDescFeignService {
    @Autowired
    private CommonRedisItemDescService commonRedisItemDescService;

    /**
     * 缓存商品介绍信息
     * @param tbItemDesc 商品介绍对象
     */
    @Override
    public Integer insertItemDesc(@RequestBody TbItemDesc tbItemDesc) {
        return this.commonRedisItemDescService.insertItemDesc(tbItemDesc);
    }

    /**
     * 查询缓存中的商品介绍
     * @param tbItemId 商品主键
     */
    @Override
    public TbItemDesc selectItemDesc(@RequestParam Long tbItemId) {
        return this.commonRedisItemDescService.selectItemDesc(tbItemId);
    }
}
