package com.bjsxt.common.redis.controller;

import com.bjsxt.common.redis.service.CommonRedisOrderService;
import com.bjsxt.feign.redis.CommonRedisOrderFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommonRedisOrderController implements CommonRedisOrderFeignService {
    @Autowired
    private CommonRedisOrderService commonRedisOrderService;
    /**
     * 通过Redis生成订单ID策略
     */
    @Override
    public Long selectOrderId() {
        return this.commonRedisOrderService.selectOrderId();
    }
}
