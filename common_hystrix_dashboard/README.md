  * Hystrix-dashboard 监控中心
  * 		步骤1.修改POM文件,添加Hystrix,actuator,Hystrix-dashboard的坐标依赖
  * 		步骤2.修改全局配置文件,修改应用名称,修改端口号,注意不能与监控的项目同PORT
  * 		步骤3.修改启动类,添加@EnableHystrix,@EnableHystrixDashboard注释开启可视化的数据监控
  *
  * 监控中心URL:SpringBoot1.x:  ip:port/hystrix
                SpringBoot2.x:  ip:port/actuator/hystrix
  * 被监控的URL:SpringBoot1.x:  ip:port/hystrix.stream
                SpringBoot2.x:  ip:port/actuator/hystrix.stream


 * 使用 Turbine 在多个服务与集群情况下收集数据监控
 * 	Turbine 是聚合服务器发送事件流数据的一个工具，hystrix的监控中，只能监控单个节点，实际生产中都为集群，
 * 	因此可以通过 turbine 来监控集群服务
 * 		步骤1.修改POM文件,添加两个turbine的坐标依赖
 * 		步骤2.修改全局配置文件,修改应用名称和端口号,以及添加对Turbine的配置
 * 		步骤3.修改启动类,添加@EnableTurbine注释,开启Turbine
 *
 *  turbine 整合服务后的监控数据URL地址:
 *  		http://ip:port/turbine.stream


* 采用 RabbitMQ，收集监控数据
* 		步骤1.修改POM文件,添加坐标依赖
* 		步骤2.修改全局配置文件,修改应用名称和端口号,以及添加对RabbitMQ的配置
* 		步骤3.修改启动类,添加@EnableTurbineStream注释,开启TurbineStream
*
*  turbine 整合服务后的监控数据URL地址:
*  		http://ip:port/turbine.stream