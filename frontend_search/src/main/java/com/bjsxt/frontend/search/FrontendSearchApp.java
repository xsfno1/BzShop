package com.bjsxt.frontend.search;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.bjsxt.mapper")
public class FrontendSearchApp {
    public static void main(String[] args) {
        SpringApplication.run(FrontendSearchApp.class, args);
    }
}
