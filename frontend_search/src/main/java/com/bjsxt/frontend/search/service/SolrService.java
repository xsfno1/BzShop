package com.bjsxt.frontend.search.service;

import com.bjsxt.entity.Result;
import com.bjsxt.entity.SolrDocumentResult;
import com.bjsxt.pojo.SolrItem;

import java.util.List;

public interface SolrService {
    Result importSolrData();

    List<SolrDocumentResult> selectByq(String q, Long page, Integer rows);

    Result clearSolrData();

    Result deleteSolrDataById(String[] ids);

    Result addSolrData(SolrItem solrItem);
}
