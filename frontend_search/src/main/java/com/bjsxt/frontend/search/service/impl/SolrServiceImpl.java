package com.bjsxt.frontend.search.service.impl;

import com.bjsxt.entity.Result;
import com.bjsxt.entity.SolrDocumentResult;
import com.bjsxt.frontend.search.service.SolrService;
import com.bjsxt.mapper.SolrItemMapper;
import com.bjsxt.pojo.SolrItem;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.query.*;
import org.springframework.data.solr.core.query.result.HighlightEntry;
import org.springframework.data.solr.core.query.result.HighlightPage;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class SolrServiceImpl implements SolrService {
    @Autowired
    private SolrItemMapper solrItemMapper;
    @Autowired
    private SolrTemplate solrTemplate;
    @Value("${spring.data.solr.core}")
    private String SOLR_COLLECTION;     //Solr索引库

    /**
     * 向 Solr 索引库中导入数据
     */
    @Override
    public Result importSolrData() {
        try {
            //查询数据
            List<SolrItem> list = this.solrItemMapper.getSolrItemList();
            //将数据添加索引库中
            SolrInputDocument document = null;
            for (SolrItem solrItem : list) {
                //创建 SolrInputDocument 对象
                document = new SolrInputDocument();
                document.setField("id", solrItem.getId());
                document.setField("item_title", solrItem.getTitle());
                document.setField("item_sell_point", solrItem.getSell_point());
                document.setField("item_price", solrItem.getPrice());
                document.setField("item_image", solrItem.getImage());
                document.setField("item_category_name", solrItem.getName());
                document.setField("item_desc", solrItem.getItem_desc());
                //写入索引库
                this.solrTemplate.saveDocument(this.SOLR_COLLECTION, document);
            }
            //提交索引库
            this.solrTemplate.commit(this.SOLR_COLLECTION);
            return Result.ok();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.error("导入数据失败");
    }

    /**
     * 搜索数据
     * @param q    关键字查询条件
     * @param page 当前页
     * @param rows 页显示数
     */
    @Override
    public List<SolrDocumentResult> selectByq(String q, Long page, Integer rows) {
        //设置高亮查询条件
        HighlightQuery query = new SimpleHighlightQuery();
        Criteria criteria = new Criteria("item_keywords");
        criteria.is(q);
        query.addCriteria(criteria);

        //设置高亮属性
        HighlightOptions highlightOptions = new HighlightOptions();
        //设置高亮显示的域
        highlightOptions.addField("item_title");
        //设置高亮的样式的前缀
        highlightOptions.setSimplePrefix("<em style='color:red;'>");
        //设置高亮的样式的后缀
        highlightOptions.setSimplePostfix("</em>");

        //在高亮查询条件中设置高亮属性
        query.setHighlightOptions(highlightOptions);

        //分页
        query.setOffset(((page - 1) * rows));    //开始下标
        query.setRows(rows);                     //页记录数

        HighlightPage<SolrDocumentResult> highlightPage =
                this.solrTemplate.queryForHighlightPage(this.SOLR_COLLECTION, query, SolrDocumentResult.class);

        // 获取总记录数
        // long totalElements = highlightPage.getTotalElements();
        // System.out.println("##############################   totalElements = "+totalElements);
        // 获取总页数
        // int pages = highlightPage.getTotalPages();
        // System.out.println("##############################   pages = "+pages);

        List<HighlightEntry<SolrDocumentResult>> highlighted = highlightPage.getHighlighted();

        for (HighlightEntry<SolrDocumentResult> tbItemHighlightEntry : highlighted) {
            //原始的实体对象
            SolrDocumentResult entity = tbItemHighlightEntry.getEntity();
            //获取高亮数据
            List<HighlightEntry.Highlight> highlights = tbItemHighlightEntry.getHighlights();
            //如果有高亮， 就取高亮
            if (highlights != null && highlights.size() > 0 && highlights.get(0).getSnipplets().size() > 0) {
                entity.setItem_title(highlights.get(0).getSnipplets().get(0));
                entity.setItem_sell_point(highlights.get(0).getSnipplets().get(0));
            }
        }
        List<SolrDocumentResult> list = highlightPage.getContent();
        return list;
    }
    /**
     * 删除Solr索引库数据
     */
    @Override
    public Result clearSolrData() {
        try{
            SimpleQuery query=new SimpleQuery("*:*");
            this.solrTemplate.delete(this.SOLR_COLLECTION,query);
            this.solrTemplate.commit(this.SOLR_COLLECTION);

            return Result.ok();
        }catch (Exception e){
            e.printStackTrace();
        }
        return Result.error("清空索引库失败");
    }
    /**
     * 按主键删除Solr索引库数据，支持多条删除
     */
    @Override
    public Result deleteSolrDataById(String[] ids) {
        try{
            this.solrTemplate.deleteByIds(this.SOLR_COLLECTION, Arrays.asList(ids));
            this.solrTemplate.commit(this.SOLR_COLLECTION);

            return Result.ok();
        }catch (Exception e){
            e.printStackTrace();
        }
        return Result.error("删除索引库失败");
    }
    /**
     * 新增Solr索引库数据
     */
    @Override
    public Result addSolrData(SolrItem solrItem) {
        try{
            //创建 SolrInputDocument 对象
            SolrInputDocument document = new SolrInputDocument();
            document.addField("id", solrItem.getId());
            document.addField("item_title", solrItem.getTitle());
            document.addField("item_sell_point", solrItem.getSell_point());
            document.addField("item_price", solrItem.getPrice());
            document.addField("item_image", solrItem.getImage());
            document.addField("item_category_name", solrItem.getName());
            document.addField("item_desc", solrItem.getItem_desc());
            //写入索引库
            this.solrTemplate.saveDocument(this.SOLR_COLLECTION, document);
            //提交
            this.solrTemplate.commit(this.SOLR_COLLECTION);

            return Result.ok();
        }catch (Exception e){
            e.printStackTrace();
        }
        return Result.error("新增索引库失败");
    }
}

