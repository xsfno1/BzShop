package com.bjsxt.frontend.search.controller;

import com.bjsxt.entity.Result;
import com.bjsxt.entity.SolrDocumentResult;
import com.bjsxt.frontend.search.service.SolrService;
import com.bjsxt.pojo.SolrItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/search", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class SolrController {
    @Autowired
    private SolrService solrService;

    /**
     * 向 Solr 索引库中导入数据库数据
     */
    @GetMapping("/importSolrData")
    public Result importSolrData() {
        try {
            return this.solrService.importSolrData();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.error("error");
    }

    /**
     * 搜索数据
     * @param q    查询条件
     * @param page 当前页
     * @param rows 页显示数
     */
    @RequestMapping("/list")
    public List<SolrDocumentResult> selectByq(@RequestParam(defaultValue = "") String q,
                                              @RequestParam(defaultValue = "1") Long page,   //Solr要求类型为Long
                                              @RequestParam(defaultValue = "10") Integer rows) //Solr要求类型为Integer
    {
        try {
            return this.solrService.selectByq(q, page, rows);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 清空Solr索引库所有数据
     */
    @GetMapping("/clear")
    public Result clearSolrData(){
        try {
            return this.solrService.clearSolrData();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.error("error");
    }
    /**
     * 按主键删除Solr索引库数据
     */
    @GetMapping("/deleteSolrDataById")
    public Result deleteSolrDataById(String[] ids){
        try {
            return this.solrService.deleteSolrDataById(ids);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.error("error");
    }
    /**
     * 新增Solr索引库数据
     */
    @GetMapping("/addSolrData")
    public Result addSolrData(SolrItem solrItem){
        try {
            return this.solrService.addSolrData(solrItem);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.error("error");
    }
}
