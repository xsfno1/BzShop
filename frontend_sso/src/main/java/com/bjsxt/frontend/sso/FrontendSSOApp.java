package com.bjsxt.frontend.sso;

import com.codingapi.txlcn.tc.config.EnableDistributedTransaction;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@MapperScan("com.bjsxt.mapper")
@EnableDistributedTransaction   //Tx-Lcn
public class FrontendSSOApp {
    public static void main(String[] args) {
        SpringApplication.run(FrontendSSOApp.class,args);
    }
}
