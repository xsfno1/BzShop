package com.bjsxt.frontend.sso.service;

import com.bjsxt.entity.Result;
import com.bjsxt.pojo.TbUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public interface FrontendSsoService {
    Result checkUserInfo(String checkValue, Integer checkFlag);

    Result userRegister(TbUser tbUser);

    Result userLogin(String username , String password, HttpServletRequest request, HttpServletResponse response);

    Result logOut(String token);
}
