package com.bjsxt.frontend.sso.controller;

import com.bjsxt.entity.Result;
import com.bjsxt.frontend.sso.service.FrontendSsoService;
import com.bjsxt.pojo.TbUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 单点登录服务接口
 */
@RestController
@RequestMapping(value = "/sso", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class FrontendSSOController {
    @Autowired
    private FrontendSsoService frontendSsoService;
    /**
     * 注册验证 ：对用户的注册信息(用户名与电话号码)做数据校验
     * @param checkValue 验证内容
     * @param checkFlag 1:验证用户名重复  2:验证手机号重复
     */
    @GetMapping("/checkUserInfo/{checkValue}/{checkFlag}")
    public Result checkUserInfo(@PathVariable String checkValue , @PathVariable Integer checkFlag) {
        try {
            return this.frontendSsoService.checkUserInfo(checkValue,checkFlag);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.build(500,"error");
    }
    /**
     * 实现用户注册
     * @param tbUser 用户对象
     */
    @PostMapping("/userRegister")
    public Result userRegister(TbUser tbUser) {
        try {
            return this.frontendSsoService.userRegister(tbUser);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.build(500,"error");
    }
    /**
     * 实现用户登录
     * @param username 用户名(手机号)
     * @param password 密码
     */
    @PostMapping("/userLogin")
    public Result userLogin(String username , String password, HttpServletRequest request, HttpServletResponse response) {
        try {
            return this.frontendSsoService.userLogin(username,password,request,response);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.build(500,"error");
    }
    /**
     * 实现退出登录
     * @param token 用户令牌
     */
    @PostMapping("/logOut")
    public Result logOut(String token) {
        try {
            return this.frontendSsoService.logOut(token);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.build(500,"error");
    }
}
