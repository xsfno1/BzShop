package com.bjsxt.frontend.sso.service.impl;

import com.bjsxt.entity.CartItem;
import com.bjsxt.entity.LoginResult;
import com.bjsxt.entity.Result;
import com.bjsxt.frontend.sso.feign.CommonRedisCartFeignClient;
import com.bjsxt.frontend.sso.feign.CommonRedisUserFeignClient;
import com.bjsxt.frontend.sso.service.FrontendSsoService;
import com.bjsxt.mapper.TbUserMapper;
import com.bjsxt.pojo.TbUser;
import com.bjsxt.pojo.TbUserExample;
import com.bjsxt.utils.CookieUtils;
import com.bjsxt.utils.JsonUtils;
import com.bjsxt.utils.MD5Utils;
import com.codingapi.txlcn.tc.annotation.LcnTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@Service
public class FrontendSsoServiceImpl implements FrontendSsoService {
    @Autowired
    private TbUserMapper tbUserMapper;
    @Autowired
    private CommonRedisUserFeignClient commonRedisUserFeignClient;
    @Value("${cart_cookie_name}")
    private String cart_cookie_name;
    @Autowired
    private CommonRedisCartFeignClient commonRedisCartFeignClient;
    /**
     * 注册验证 ：对用户的注册信息(用户名与电话号码)做数据校验
     * @param checkValue 验证内容
     * @param checkFlag 1:验证用户名重复  2:验证手机号重复
     */
    @Override
    public Result checkUserInfo(String checkValue, Integer checkFlag) {
        TbUserExample example = new TbUserExample();
        TbUserExample.Criteria criteria = example.createCriteria();
        if(checkFlag == 1){
            // 校验用户名
            criteria.andUsernameEqualTo(checkValue);
        } else if(checkFlag == 2){
            // 校验电话号码
            criteria.andPhoneEqualTo(checkValue);
        } else{
            // 校验有误
            return Result.error("验证内容有误");
        }
        int count = this.tbUserMapper.countByExample(example);
        // 数据库中存在数据
        if (count > 0){
            return Result.error("重复");
        }
        return Result.ok();
    }
    /**
     * 实现用户注册
     * @param tbUser 用户对象
     */
    @Override
    @LcnTransaction
    public Result userRegister(TbUser tbUser) {
        // 1.将密码做加密处理
        String md5Pwd = MD5Utils.digest(tbUser.getPassword());
        tbUser.setPassword(md5Pwd);
        // 2.数据补全
        Date date = new Date();
        tbUser.setCreated(date);
        tbUser.setUpdated(date);
        // 3.插入数据库
        int result = this.tbUserMapper.insert(tbUser);
        if(result == 1){
            return Result.ok();
        }
        return Result.error("用户注册失败");
    }

    /**
     * 实现用户登录
     * @param username 用户名(手机号)
     * @param password 密码
     */
    @Override
    public Result userLogin(String username , String password , HttpServletRequest request, HttpServletResponse response) {
        // 1.将前端的密码进行加密处理
        String md5Pwd = MD5Utils.digest(password);
        // 2.与数据库比对
        TbUserExample example = new TbUserExample();
        example.or().andUsernameEqualTo(username);
        example.or().andPhoneEqualTo(username);
        List<TbUser> users = this.tbUserMapper.selectByExample(example);
        // 3.结果
        if(users != null && users.size() > 0 && users.size() <= 2){
            for (TbUser tbUser:users) {
                // 4.登录成功：保存用户登录状态
                if(tbUser.getPassword().equals(md5Pwd)){
                    // 4.1生成随机ID作为Redis的KEY
                    String token = UUID.randomUUID().toString();
                    // 4.2添加到Redis
                    Integer flag = this.commonRedisUserFeignClient.addUserToRedis(tbUser, token);
                    if(flag == 200){
                        // 4.3封装登录用户模型
                        LoginResult loginResult = new LoginResult();
                        loginResult.setToken(token);
                        loginResult.setUserid(tbUser.getId().toString());
                        loginResult.setUsername(tbUser.getUsername());

                        // 4.4实现同步购物车
                        this.syncCart(tbUser.getId().toString(),request,response);

                        // 4.5返回
                        return Result.ok(loginResult);
                    }
                }
            }
            return Result.error("登录失败");
        }
        return Result.error("用户不存在");
    }

    /**
     * 实现登录后同步购物车
     * @param userId
     * @param request
     */
    private Integer syncCart(String userId, HttpServletRequest request, HttpServletResponse response) {
        // 1.获取Cookie中临时购物车对象
        Map<String, CartItem> cookieCart = this.getCart(request);
        // 2.获取永久购物车
        Map<String,CartItem> redisCart = this.getCart(userId);
        // 3.删除永久购物车中所包含临时购物车中的商品
        for (String key:cookieCart.keySet()) {
            redisCart.remove(key);
        }
        // 4.将Cookie临时购物车同步到Redis永久购物车中
        redisCart.putAll(cookieCart);
        // 5.将永久购物车重新缓存到 redis 中
        this.addCartToRedis(userId,redisCart);
        // 6.删除Cookie中临时购物车
        CookieUtils.deleteCookie(request,response,this.cart_cookie_name);
        return 200;
    }

    /**
     * 1.获取Cookie中临时购物车对象
     */
    private Map<String, CartItem> getCart(HttpServletRequest request) {
        try {
            // 获取Cookie值
            String cookieCart = CookieUtils.getCookieValue(request, this.cart_cookie_name,true);
            if(!StringUtils.isEmpty(cookieCart)){
                // 类型转换
                Map<String, CartItem> map = JsonUtils.jsonToMap(cookieCart, CartItem.class);
                if(map != null){
                    return map;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new HashMap<String, CartItem>();
    }
    /**
     * 2.获取Redis中永久购物车对象
     */
    private Map<String, CartItem> getCart(String userId) {
        try {
            Map<String, CartItem> cart = this.commonRedisCartFeignClient.selectCartByUserId(userId);
            if(cart != null){
                return cart;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new HashMap<>();
    }
    /**
     * 5.将永久购物车重新缓存到 redis 中
     */
    private void addCartToRedis(String userId, Map<String, CartItem> redisCart) {
        Map<String, Object> map = new HashMap<>();
        map.put("userId",userId);
        map.put("cart",redisCart);
        this.commonRedisCartFeignClient.insertCart(map);
    }

    /**
     * 实现退出登录
     * @param token 用户令牌
     */
    @Override
    public Result logOut(String token) {
        Integer flag = this.commonRedisUserFeignClient.deleteUserFromRedis(token);
        if(flag == 200){
            return Result.ok();
        }
        return Result.error("退出失败");
    }
}
