package com.bjsxt.frontend.sso.feign;

import com.bjsxt.feign.redis.CommonRedisCartFeignService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("common-redis")
public interface CommonRedisCartFeignClient extends CommonRedisCartFeignService {
}
