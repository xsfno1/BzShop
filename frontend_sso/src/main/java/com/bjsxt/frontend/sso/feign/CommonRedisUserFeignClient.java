package com.bjsxt.frontend.sso.feign;

import com.bjsxt.feign.redis.CommonRedisUserFeignService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("common-redis")
public interface CommonRedisUserFeignClient extends CommonRedisUserFeignService {
}
