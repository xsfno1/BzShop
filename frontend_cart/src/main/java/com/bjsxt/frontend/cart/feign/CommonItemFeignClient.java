package com.bjsxt.frontend.cart.feign;

import com.bjsxt.feign.CommonItemFeignService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("common-item")
public interface CommonItemFeignClient extends CommonItemFeignService {
}
