package com.bjsxt.frontend.cart.controller;

import com.bjsxt.entity.Result;
import com.bjsxt.frontend.cart.service.CookieCartService;
import com.bjsxt.frontend.cart.service.RedisCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping(value = "/cart", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class FrontendCartController {
    @Autowired
    private CookieCartService cookieCartService;  //处理 用户未登录 业务层
    @Autowired
    private RedisCartService redisCartService;  //处理 用户已登录 业务层

    /**
     * 将商品加入购物车
     * @param userId 用户主键
     * @param itemId 商品主键
     * @param num 商品数量
     * @param request 请求对象
     * @param response 响应对象
     */
    @GetMapping("/addItem")
    public Result addItemToCart(@RequestParam(required = false) Long userId ,
                                Long itemId ,
                                @RequestParam(defaultValue = "1") Integer num ,
                                HttpServletRequest request,
                                HttpServletResponse response){
        try {
            //用户未登录
            if(userId == null){
                return this.cookieCartService.addItemToCart(itemId,num,request,response);
            }
            //用户已登录
            return this.redisCartService.addItemToCart(itemId,num,userId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.build(500,"error");
    }

    /**
     * 查看购物车
     * @param userId 用户主键
     * @param request 请求对象
     * @param response 响应对象
     */
    @PostMapping("/showCart")
    public Result showCart(@RequestParam(required = false) Long userId ,
                                HttpServletRequest request,
                                HttpServletResponse response){
        try {
            //用户未登录
            if(userId == null){
                return this.cookieCartService.showCart(request,response);
            }
            //用户已登录
            return this.redisCartService.showCart(userId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.build(500,"error");
    }
    /**
     * 修改购物车中商品的数量
     */
    @PostMapping("/updateItemNum")
    public Result updateItemNum(Long itemId,
                                @RequestParam(required = false) Long userId,
                                Integer num,
                                HttpServletRequest request,
                                HttpServletResponse response){
        try {
            //用户未登录
            if(userId == null){
                return this.cookieCartService.updateItemNum(itemId,num,request,response);
            }
            //用户已登录
            return this.redisCartService.updateItemNum(userId,itemId,num);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.build(500,"error");
    }

    /**
     * 删除购物车中的商品
     * @param itemId 商品主键
     * @param userId 用户主键
     * @param request 请求对象
     * @param response 响应对象
     */
    @PostMapping("/deleteItemFromCart")
    public Result deleteItemFromCart(@RequestParam Long itemId,
                                     @RequestParam(required = false) Long userId,
                                     HttpServletRequest request,
                                     HttpServletResponse response){
        try {
            //用户未登录
            if(userId == null){
                return this.cookieCartService.deleteItemFromCart(itemId,request,response);
            }
            //用户已登录
            return this.redisCartService.deleteItemFromCart(itemId,userId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.build(500,"error");
    }

    /**
     * 实现结算接口
     * @param ids 商品主键集合
     * @param userId 用户主键
     */
    @PostMapping("/goSettlement")
    public Result goSettlement(String[] ids, String userId){
        try {
            if(StringUtils.isEmpty(userId)){
                return Result.build(500,"未登录");
            }
            return this.redisCartService.goSettlement(ids,userId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.build(500,"error");
    }
}
