package com.bjsxt.frontend.cart.service.impl;

import com.bjsxt.frontend.cart.feign.CommonRedisSSOFeignClient;
import com.bjsxt.frontend.cart.service.UserCheckService;
import com.bjsxt.pojo.TbUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserCheckServiceImpl implements UserCheckService {
    @Autowired
    private CommonRedisSSOFeignClient commonRedisSSOFeignClient;
    /**
     * 判断用户登录状态有效性
     * @param token 用户令牌
     */
    @Override
    public TbUser checkUserToken(String token) {
        return this.commonRedisSSOFeignClient.checkUserToken(token);
    }
}
