package com.bjsxt.frontend.cart.service.impl;

import com.bjsxt.entity.CartItem;
import com.bjsxt.entity.Result;
import com.bjsxt.frontend.cart.feign.CommonItemFeignClient;
import com.bjsxt.frontend.cart.service.CookieCartService;
import com.bjsxt.pojo.TbItem;
import com.bjsxt.utils.CookieUtils;
import com.bjsxt.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 处理 用户未登录 业务层
 */
@Service
public class CookieCartServiceImpl implements CookieCartService {
    @Value("${cart_cookie_name}")
    private String cart_cookie_name; //Cookie中购物车的KEY
    @Autowired
    private CommonItemFeignClient commonItemFeignClient;
    /**
     * 将商品加入购物车
     * @param itemId 商品主键
     * @param num 商品数量
     * @param request 请求对象
     * @param response 响应对象
     */
    @Override
    public Result addItemToCart(Long itemId, Integer num, HttpServletRequest request, HttpServletResponse response) {
        // 1，获取购物车对象
        Map<String, CartItem> cart = this.getCartObj(request);
        // 2，查询商品
        TbItem tbItem = this.selectItemById(itemId);
        // 3，向购物车中添加商品
        this.doAddItemToCart(cart,tbItem,num);
        // 4，将购物车通过 Cookie 写回给客户端浏览器
        this.sendClientCookie(cart,request,response);
        return Result.ok();
    }

    /**
     * 1，获取购物车对象
     */
    private Map<String , CartItem> getCartObj(HttpServletRequest request){
        //获取Cookie中临时购物车对象
        String cookieValue = CookieUtils.getCookieValue(request, this.cart_cookie_name, true);
        //临时购物车对象已存在，返回
        if (!StringUtils.isEmpty(cookieValue)) {
            try {
                // json 转换 Map
                return JsonUtils.jsonToMap(cookieValue, CartItem.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //临时购物车对象不存在，创建新对象
        return new HashMap<>();
    }
    /**
     * 2,根据商品 ID 查询商品
     */
    private TbItem selectItemById(Long itemId){
        return this.commonItemFeignClient.selectItemInfo(itemId);
    }
    /**
     * 3，向购物车中添加商品
     */
    private void doAddItemToCart(Map<String, CartItem> cart, TbItem tbItem, Integer num) {
        Long itemId = tbItem.getId();
        // 从购物车中取商品
        CartItem cartItem = cart.get(itemId.toString());
        // 购物车中不存在该商品
        if(cartItem == null) {
            cartItem = new CartItem();
            cartItem.setId(itemId);
            cartItem.setImage(tbItem.getImage());
            cartItem.setNum(num);
            cartItem.setPrice(tbItem.getPrice());
            cartItem.setTitle(tbItem.getTitle());

            cart.put(itemId.toString(),cartItem);
        } else{
            // 购物车中已存在该商品，数量 + num
            cartItem.setNum(cartItem.getNum() + num);
        }
    }
    /**
     * 4,将购物车通过 Cookie 写回给客户端浏览器
     */
    private void sendClientCookie(Map<String,CartItem> cart,HttpServletRequest request,HttpServletResponse response){
        String cartJson = JsonUtils.objectToJson(cart);
        CookieUtils.setCookie(request,response,this.cart_cookie_name,cartJson,true);
    }
    /**
     * 查看购物车
     * @param request 请求对象
     * @param response 响应对象
     */
    @Override
    public Result showCart(HttpServletRequest request, HttpServletResponse response) {
        // 1.获取购物车对象
        Map<String, CartItem> cart = this.getCartObj(request);
        // 2.类型转换  Map  -->  List
        List<CartItem> cartItems = new ArrayList<>();
        for (String key : cart.keySet()){
            cartItems.add(cart.get(key));
        }
        return Result.ok(cartItems);
    }

    /**
     * 修改购物车中商品的数量
     * @param itemId 商品主键
     * @param num 数量
     * @param request 请求对象
     * @param response 响应对象
     */
    @Override
    public Result updateItemNum(Long itemId,Integer num, HttpServletRequest request, HttpServletResponse response) {
        // 1.获取购物车对象
        Map<String, CartItem> cart = this.getCartObj(request);
        // 2.获取购物车商品项
        CartItem cartItem = cart.get(itemId.toString());
        if(cartItem == null){
            return Result.error("并无此商品");
        }
        // 3.更改数量
        cartItem.setNum(num);
        // 4.覆盖Cookie
        this.sendClientCookie(cart,request,response);
        return Result.ok();
    }
    /**
     * 删除购物车中的商品
     * @param itemId 商品主键
     * @param request 请求对象
     * @param response 响应对象
     */
    @Override
    public Result deleteItemFromCart(Long itemId, HttpServletRequest request, HttpServletResponse response) {
        // 1.获取购物车对象
        Map<String, CartItem> cart = this.getCartObj(request);
        // 2.获取购物车中商品项对象
        CartItem cartItem = cart.get(itemId.toString());
        if(cartItem == null){
            return Result.error("并无此商品");
        }
        // 3.删除
        cart.remove(itemId.toString());
        // 4.覆盖Cookie
        this.sendClientCookie(cart,request,response);
        return Result.ok();
    }
}
