package com.bjsxt.frontend.cart.feign;

import com.bjsxt.feign.redis.CommonRedisSSOFeignService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("common-redis")
public interface CommonRedisSSOFeignClient extends CommonRedisSSOFeignService {
}
