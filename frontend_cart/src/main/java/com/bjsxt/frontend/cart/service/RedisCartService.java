package com.bjsxt.frontend.cart.service;

import com.bjsxt.entity.Result;

/**
 * 处理 用户已登录 业务层
 */
public interface RedisCartService {
    Result addItemToCart(Long itemId, Integer num, Long userId);

    Result showCart(Long userId);

    Result updateItemNum(Long userId,Long itemId, Integer num);

    Result deleteItemFromCart(Long itemId, Long userId);

    Result goSettlement(String[] ids, String userId);
}
