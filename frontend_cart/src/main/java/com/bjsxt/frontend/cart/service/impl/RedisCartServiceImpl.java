package com.bjsxt.frontend.cart.service.impl;

import com.bjsxt.entity.CartItem;
import com.bjsxt.entity.Result;
import com.bjsxt.frontend.cart.feign.CommonItemFeignClient;
import com.bjsxt.frontend.cart.feign.CommonRedisFeignClient;
import com.bjsxt.frontend.cart.service.RedisCartService;
import com.bjsxt.pojo.TbItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 处理 用户已登录 业务层
 */
@Service
public class RedisCartServiceImpl implements RedisCartService {
    @Autowired
    private CommonItemFeignClient commonItemFeignClient;
    @Autowired
    private CommonRedisFeignClient commonRedisFeignClient;
    /**
     * 将商品加入购物车  加入Redis缓存中
     * @param userId 用户主键
     * @param itemId 商品主键
     * @param num 商品数量
     */
    @Override
    public Result addItemToCart(Long itemId, Integer num, Long userId) {
        // 1，获取购物车对象
        Map<String, CartItem> cart = this.getCartObj(userId.toString());
        // 2，查询商品
        TbItem tbItem = this.selectItemById(itemId);
        // 3，向购物车中添加商品
        this.doAddItemToCart(cart,tbItem,num);
        // 4，将购物车写入Redis
        Integer flag = this.addCartToRedis(cart, userId.toString());
        if(flag == 200){
            return Result.ok();
        }
        return Result.error("加入购物车缓存失败");
    }

    /**
     * 1，获取购物车对象
     */
    private Map<String , CartItem> getCartObj(String userId){
        try {
            //获取Redis中购物车对象
            Map<String, CartItem> map = this.commonRedisFeignClient.selectCartByUserId(userId);
            //购物车对象已存在，返回
            if (map != null) {
                return map;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //临时购物车对象不存在，创建新对象
        return new HashMap<>();
    }
    /**
     * 2，查询商品
     */
    private TbItem selectItemById(Long itemId) {
        return this.commonItemFeignClient.selectItemInfo(itemId);
    }
    /**
     * 3，向购物车中添加商品
     */
    private void doAddItemToCart(Map<String, CartItem> cart, TbItem tbItem, Integer num){
        //获取商品ID
        String itemId = tbItem.getId().toString();
        //判断购物车中是否存在相同商品
        CartItem cItem = cart.get(itemId);
        if(cItem == null){
            //没有存在相同商品
            cItem = new CartItem();
            cItem.setId(tbItem.getId());
            cItem.setImage(tbItem.getImage());
            cItem.setNum(num);
            cItem.setPrice(tbItem.getPrice());
            cItem.setSellPoint(tbItem.getSellPoint());
            cItem.setTitle(tbItem.getTitle());

            cart.put(tbItem.getId().toString(),cItem);
        }else {
            //存在相同商品 ==> 数量+1
            cItem.setNum(cItem.getNum()+num);
        }
    }

    /**
     *  // 4，将购物车写入Redis
     */
    private Integer addCartToRedis(Map<String, CartItem> cart,String userId){
        Map<String,Object> map = new HashMap<>();
        map.put("userId",userId);
        map.put("cart",cart);

        return this.commonRedisFeignClient.insertCart(map);
    }
    /**
     * 查看购物车
     * @param userId 用户主键
     */
    @Override
    public Result showCart(Long userId) {
        // 从Redis中获取购物车对象
        Map<String, CartItem> cart = this.getCartObj(userId.toString());
        if(cart.size() == 0){
            return Result.ok("购物车为空");
        }
        // 封装前端返回结果
        List<CartItem> list = new ArrayList<>();
        for (String key:cart.keySet()) {
            list.add(cart.get(key));
        }
        return Result.ok(list);
    }
    /**
     * 修改购物车中商品的数量
     */
    @Override
    public Result updateItemNum(Long userId, Long itemId, Integer num) {
        // 从Redis中获取购物车对象
        Map<String, CartItem> cart = this.getCartObj(userId.toString());
        // 获取对应商品项
        CartItem cartItem = cart.get(itemId.toString());
        if (cartItem != null) {
            // 修改数量
            cartItem.setNum(num);
            // 将新的购物车缓存到 Redis 中
            Integer flag = this.addCartToRedis(cart, userId.toString());
            if(flag == 200){
                return Result.ok();
            }
        }
        return Result.error("修改数量失败");
    }
    /**
     * 删除购物车中的商品
     * @param itemId 商品主键
     * @param userId 用户主键
     */
    @Override
    public Result deleteItemFromCart(Long itemId, Long userId) {
        // 从Redis中获取购物车对象
        Map<String, CartItem> cart = this.getCartObj(userId.toString());
        // 从购物车中移除商品
        cart.remove(itemId.toString());
        //将新的购物车缓存到 Redis 中
        Integer flag = this.addCartToRedis(cart, userId.toString());
        if(flag == 200){
            return Result.ok();
        }
        return Result.error("删除商品失败");
    }
    /**
     * 实现结算接口
     * @param ids 商品主键集合
     * @param userId 用户主键
     */
    @Override
    public Result goSettlement(String[] ids, String userId) {
        // 获取购物车
        Map<String,CartItem> cart = this.getCartObj(userId);
        // 从购物车中获取选中的商品
        List list = this.getItemList(cart,ids);
        return Result.ok(list);
    }
    /**
     * 从购物车中获取选中的商品
     */
    private List<CartItem> getItemList(Map<String,CartItem> cart,String[] ids){
        List<CartItem> list = new ArrayList<>();
        for(String id:ids){
            list.add(cart.get(id));
        }
        return list;
    }
}
