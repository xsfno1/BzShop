package com.bjsxt.frontend.cart.service;

import com.bjsxt.entity.Result;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * 处理 用户未登录 业务层
 */
public interface CookieCartService {
    Result addItemToCart(Long itemId, Integer num, HttpServletRequest request, HttpServletResponse response);

    Result showCart(HttpServletRequest request, HttpServletResponse response);

    Result updateItemNum(Long itemId,Integer num, HttpServletRequest request, HttpServletResponse response);

    Result deleteItemFromCart(Long itemId, HttpServletRequest request, HttpServletResponse response);
}
