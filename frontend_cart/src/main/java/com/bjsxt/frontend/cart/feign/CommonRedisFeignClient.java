package com.bjsxt.frontend.cart.feign;

import com.bjsxt.feign.redis.CommonRedisCartFeignService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("common-redis")
public interface CommonRedisFeignClient extends CommonRedisCartFeignService {
}
