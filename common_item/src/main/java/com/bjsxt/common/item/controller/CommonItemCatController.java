package com.bjsxt.common.item.controller;

import com.bjsxt.common.item.service.CommonItemCatService;
import com.bjsxt.entity.CatResult;
import com.bjsxt.feign.CommonItemCatFeignService;
import com.bjsxt.pojo.TbItemCat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CommonItemCatController implements CommonItemCatFeignService {
    @Autowired
    private CommonItemCatService commonItemCatService;

    /**
     * 根据父节点ID查询商品类目列表
     * @param parentId 父节点ID
     * @return List<TbItemCat>
     */
    @Override
    public List<TbItemCat> selectItemCategoryByParentId(Long parentId) {
        return this.commonItemCatService.selectItemCategoryByParentId(parentId);
    }

    /**
     * 查询首页商品分类
     * @return CatResult
     */
    @Override
    public CatResult selectItemCategoryAll() {
        return this.commonItemCatService.selectItemCategoryAll();
    }
}
