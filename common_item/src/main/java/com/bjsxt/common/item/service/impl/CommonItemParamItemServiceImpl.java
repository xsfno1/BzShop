package com.bjsxt.common.item.service.impl;

import com.bjsxt.common.item.service.CommonItemParamItemService;
import com.bjsxt.mapper.TbItemParamItemMapper;
import com.bjsxt.pojo.TbItemParamItem;
import com.bjsxt.pojo.TbItemParamItemExample;
import com.codingapi.txlcn.tc.annotation.LcnTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommonItemParamItemServiceImpl implements CommonItemParamItemService {
    @Autowired
    private TbItemParamItemMapper tbItemParamItemMapper;
    @Override
    @LcnTransaction
    public Integer insertTbItemParamItem(TbItemParamItem tbItemParamItem) {
        return this.tbItemParamItemMapper.insert(tbItemParamItem);
    }
    /**
     * TbItemParamItem更新
     */
    @Override
    @LcnTransaction
    public Integer updateTbItemParamItem(TbItemParamItem tbItemParamItem) {
        TbItemParamItemExample example = new TbItemParamItemExample();
        TbItemParamItemExample.Criteria criteria = example.createCriteria();
        criteria.andItemIdEqualTo(tbItemParamItem.getItemId());
        return this.tbItemParamItemMapper.updateByExampleSelective(tbItemParamItem,example);
    }
    /**
     * 通过商品主键获取商品规格参数信息
     * @param itemId 商品主键
     */
    @Override
    public TbItemParamItem selectTbItemParamItemByItemId(Long itemId) {
        TbItemParamItemExample example = new TbItemParamItemExample();
        TbItemParamItemExample.Criteria criteria = example.createCriteria();
        criteria.andItemIdEqualTo(itemId);
        List<TbItemParamItem> tbItemParamItems = this.tbItemParamItemMapper.selectByExampleWithBLOBs(example);
        if(tbItemParamItems!=null && tbItemParamItems.size() ==1){
            return tbItemParamItems.get(0);
        }
        return null;
    }
}
