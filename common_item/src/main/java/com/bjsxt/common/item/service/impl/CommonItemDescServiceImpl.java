package com.bjsxt.common.item.service.impl;

import com.bjsxt.common.item.service.CommonItemDescService;
import com.bjsxt.mapper.TbItemDescMapper;
import com.bjsxt.pojo.TbItemDesc;
import com.bjsxt.pojo.TbItemDescExample;
import com.codingapi.txlcn.tc.annotation.LcnTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommonItemDescServiceImpl implements CommonItemDescService {
    @Autowired
    private TbItemDescMapper tbItemDescMapper;

    @Override
    @LcnTransaction
    public Integer insertTbItemDesc(TbItemDesc tbItemDesc) {
        // ###################   TxManager LCN 事务测试 (测试结果：成功) ####################
        // int i = 1/0;
        // ###################   TxManager LCN 事务测试  ####################################
        return this.tbItemDescMapper.insert(tbItemDesc);
    }
    /**
     * TbItemDesc更新
     */
    @Override
    @LcnTransaction
    public Integer updateTbItemDesc(TbItemDesc tbItemDesc) {
        // ###################   TxManager LCN 事务测试 (测试结果：成功) ####################
        // int i = 1/0;
        // ###################   TxManager LCN 事务测试  ####################################
        return this.tbItemDescMapper.updateByPrimaryKeyWithBLOBs(tbItemDesc);
    }
    /**
     * 根据商品主键获取商品详情信息
     * @param itemId 商品主键
     */
    @Override
    public TbItemDesc selectItemDescByItemId(Long itemId) {
        TbItemDescExample example = new TbItemDescExample();
        TbItemDescExample.Criteria criteria = example.createCriteria();
        criteria.andItemIdEqualTo(itemId);
        List<TbItemDesc> tbItemDescs = this.tbItemDescMapper.selectByExampleWithBLOBs(example);
        if(tbItemDescs!=null && tbItemDescs.size() == 1){
            return tbItemDescs.get(0);
        }
        return null;
    }
}
