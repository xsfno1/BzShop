package com.bjsxt.common.item.service.impl;

import com.bjsxt.common.item.service.CommonItemService;
import com.bjsxt.mapper.TbItemCatMapper;
import com.bjsxt.mapper.TbItemDescMapper;
import com.bjsxt.mapper.TbItemMapper;
import com.bjsxt.mapper.TbItemParamItemMapper;
import com.bjsxt.pojo.*;
import com.bjsxt.entity.PageResult;
import com.codingapi.txlcn.tc.annotation.LcnTransaction;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CommonItemServiceImpl implements CommonItemService {
    @Autowired
    private TbItemMapper tbItemMapper;
    @Autowired
    private TbItemDescMapper tbItemDescMapper;
    @Autowired
    private TbItemCatMapper tbItemCatMapper;
    @Autowired
    private TbItemParamItemMapper tbItemParamItemMapper;

    /**
     * 分页查询商品列表
     * @param page 当前页
     * @param rows 每页显示数
     */
    @Override
    public PageResult selectTbItemAllByPage(Integer page, Integer rows) {
        PageHelper.startPage(page,rows);

        TbItemExample example = new TbItemExample();
        TbItemExample.Criteria criteria = example.createCriteria();
        criteria.andStatusEqualTo((byte) 1);
        List<TbItem> itemList = this.tbItemMapper.selectByExample(example);

        PageInfo<TbItem> pageInfo = new PageInfo<>(itemList);

        PageResult result = new PageResult();
        result.setPageIndex(page);
        result.setTotalPage(pageInfo.getTotal());
        result.setResult(itemList);
        return result;
    }
    /**
     * 商品添加
     * @param tbItem 商品对象
     * @return Integer 成功返回1
     */
    @Override
    @LcnTransaction
    public Integer insertTbItem(TbItem tbItem) {
        return this.tbItemMapper.insert(tbItem);
    }
    /**
     * 更新商品tbItem
     * @param tbItem 商品对象
     */
    @Override
    public Integer updateItemById(TbItem tbItem) {
        return this.tbItemMapper.updateByPrimaryKeySelective(tbItem);
    }
    /**
     * 根据商品 ID 查询商品所有信息
     * @param itemId 商品ID
     */
    @Override
    public Map<String, Object> preUpdateItem(Long itemId) {
        Map<String,Object> map = new HashMap<>();

        //根据商品 ID 查询商品
        TbItem tbItem = this.tbItemMapper.selectByPrimaryKey(itemId);

        //根据商品 ID 查询商品描述
        TbItemDesc tbItemDesc = this.tbItemDescMapper.selectByPrimaryKey(itemId);

        //根据商品 ID 查询商品类目
        TbItemCat tbItemCat = this.tbItemCatMapper.selectByPrimaryKey(tbItem.getCid());

        //根据商品 ID 查询商品规格参数
        TbItemParamItemExample example = new TbItemParamItemExample();
        TbItemParamItemExample.Criteria criteria = example.createCriteria();
        criteria.andItemIdEqualTo(itemId);
        List<TbItemParamItem> list = this.tbItemParamItemMapper.selectByExampleWithBLOBs(example);

        map.put("item",tbItem);
        map.put("itemDesc",tbItemDesc.getItemDesc());
        map.put("itemCat",tbItemCat.getName());
        if(list != null && list.size() > 0){
            map.put("itemParamItem",list.get(0).getParamData());
        }

        return map;
    }
    /**
     * TbItem更新
     */
    @Override
    @LcnTransaction
    public Integer updateTbItem(TbItem tbItem) {
        return this.tbItemMapper.updateByPrimaryKeySelective(tbItem);
    }
    /**
     * 根据商品主键获取商品信息
     * @param itemId 商品主键
     */
    @Override
    public TbItem selectItemInfo(Long itemId) {
        return this.tbItemMapper.selectByPrimaryKey(itemId);
    }
}
