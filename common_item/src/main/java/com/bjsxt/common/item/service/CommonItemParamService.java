package com.bjsxt.common.item.service;

import com.bjsxt.entity.PageResult;
import com.bjsxt.pojo.TbItemParam;

public interface CommonItemParamService {
    TbItemParam selectItemParamByItemCatId(Long itemCatId);

    PageResult selectItemParamAll(Integer page, Integer rows);

    Integer insertItemParam(TbItemParam tbItemParam);

    Integer deleteItemParamById(Long id);

}
