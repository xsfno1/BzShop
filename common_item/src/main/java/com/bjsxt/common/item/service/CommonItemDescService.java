package com.bjsxt.common.item.service;

import com.bjsxt.pojo.TbItemDesc;

public interface CommonItemDescService {
    Integer insertTbItemDesc(TbItemDesc tbItemDesc);

    Integer updateTbItemDesc(TbItemDesc tbItemDesc);

    TbItemDesc selectItemDescByItemId(Long itemId);

}
