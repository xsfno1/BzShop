package com.bjsxt.common.item.service.impl;

import com.bjsxt.common.item.service.CommonItemParamService;
import com.bjsxt.entity.PageResult;
import com.bjsxt.mapper.TbItemParamMapper;
import com.bjsxt.pojo.TbItemParam;
import com.bjsxt.pojo.TbItemParamExample;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommonItemParamServiceImpl implements CommonItemParamService {
    @Autowired
    private TbItemParamMapper tbItemParamMapper;

    @Override
    public TbItemParam selectItemParamByItemCatId(Long itemCatId) {
        TbItemParamExample example = new TbItemParamExample();
        TbItemParamExample.Criteria criteria = example.createCriteria();
        criteria.andItemCatIdEqualTo(itemCatId);
        List<TbItemParam> list = this.tbItemParamMapper.selectByExampleWithBLOBs(example);
        if (list != null && list.size() == 1) {
            return list.get(0);
        }
        return null;
    }

    /**
     * 查询所有规格参数模板
     * @param page 当前页
     * @param rows 页显示数
     */
    @Override
    public PageResult selectItemParamAll(Integer page, Integer rows) {
        PageHelper.startPage(page,rows);

        TbItemParamExample example = new TbItemParamExample();
        List<TbItemParam> list = this.tbItemParamMapper.selectByExampleWithBLOBs(example);

        PageInfo<TbItemParam> pageInfo = new PageInfo<>(list);

        PageResult pageResult = new PageResult();
        pageResult.setPageIndex(page);
        pageResult.setTotalPage(pageInfo.getTotal());
        pageResult.setResult(list);

        return pageResult;
    }
    /**
     * 规格参数添加
     * @param tbItemParam 规格参数对象
     */
    @Override
    public Integer insertItemParam(TbItemParam tbItemParam) {
        return this.tbItemParamMapper.insert(tbItemParam);
    }
    /**
     * 规格参数删除
     * @param id 主键
     */
    @Override
    public Integer deleteItemParamById(Long id) {
        return this.tbItemParamMapper.deleteByPrimaryKey(id);
    }
}
