package com.bjsxt.common.item.service;

import com.bjsxt.entity.CatResult;
import com.bjsxt.pojo.TbItemCat;

import java.util.List;

public interface CommonItemCatService {
    List<TbItemCat> selectItemCategoryByParentId(Long parentId);

    CatResult selectItemCategoryAll();
}
