package com.bjsxt.common.item.controller;

import com.bjsxt.common.item.service.CommonItemDescService;
import com.bjsxt.feign.CommonItemDescFeignService;
import com.bjsxt.pojo.TbItemDesc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommonItemDescController implements CommonItemDescFeignService {
    @Autowired
    private CommonItemDescService commonItemDescService;

    /**
     * 数据库插入商品详情
     * @param tbItemDesc 商品详情对象
     * @return Integer
     */
    @Override
    public Integer insertTbItemDesc(@RequestBody TbItemDesc tbItemDesc) {
        return this.commonItemDescService.insertTbItemDesc(tbItemDesc);
    }

    /**
     * TbItemDesc更新
     */
    @Override
    public Integer updateTbItemDesc(@RequestBody TbItemDesc tbItemDesc) {
        return this.commonItemDescService.updateTbItemDesc(tbItemDesc);
    }
}
