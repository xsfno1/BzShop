package com.bjsxt.common.item;

import com.codingapi.txlcn.tc.config.EnableDistributedTransaction;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.bjsxt.mapper")
@EnableDistributedTransaction   //开启 TX-LCN
public class CommonItemApp {
    public static void main(String[] args) {
        SpringApplication.run(CommonItemApp.class,args);
    }
}
