package com.bjsxt.common.item.controller;

import com.bjsxt.common.item.service.CommonItemDescService;
import com.bjsxt.common.item.service.CommonItemParamItemService;
import com.bjsxt.common.item.service.CommonItemService;
import com.bjsxt.entity.PageResult;
import com.bjsxt.feign.CommonItemFeignService;
import com.bjsxt.pojo.TbItem;
import com.bjsxt.pojo.TbItemDesc;
import com.bjsxt.pojo.TbItemParamItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class CommonItemController implements CommonItemFeignService {
    @Autowired
    private CommonItemService itemService;
    @Autowired
    private CommonItemDescService commonItemDescService;
    @Autowired
    private CommonItemParamItemService commonItemParamItemService;
    /**
     * 分页查询商品列表
     * @param page 当前页
     * @param rows 每页显示数
     */
    @Override
    public PageResult selectTbItemAllByPage(Integer page, Integer rows){
        return this.itemService.selectTbItemAllByPage(page,rows);
    }

    /**
     * 商品添加
     * @param tbItem 商品对象
     * @return Integer 成功返回1
     */
    @Override
    public Integer insertTbItem(@RequestBody TbItem tbItem) {
        return this.itemService.insertTbItem(tbItem);
    }

    /**
     * 删除TbItem表记录，将商品状态修改为 3：删除
     * @param tbItem 商品
     */
    @Override
    public Integer deleteItemById(@RequestBody TbItem tbItem) {
        return this.itemService.updateItemById(tbItem);
    }

    /**
     * 预更新商品接口
     * @param itemId 商品ID
     */
    @Override
    public Map<String, Object> preUpdateItem(Long itemId) {
        return this.itemService.preUpdateItem(itemId);
    }

    /**
     * TbItem更新
     */
    @Override
    public Integer updateTbItem(@RequestBody TbItem tbItem) {
        return this.itemService.updateTbItem(tbItem);
    }

    /**
     * 根据商品主键获取商品信息
     * @param itemId 商品主键
     */
    @Override
    public TbItem selectItemInfo(@RequestParam Long itemId) {
        return this.itemService.selectItemInfo(itemId);
    }
    /**
     * 通过商品主键获取商品详情信息
     * @param itemId 商品主键
     */
    @Override
    public TbItemDesc selectItemDescByItemId(@RequestParam Long itemId) {
        return this.commonItemDescService.selectItemDescByItemId(itemId);
    }
    /**
     * 通过商品主键获取商品规格参数信息
     * @param itemId 商品主键
     */
    @Override
    public TbItemParamItem selectTbItemParamItemByItemId(@RequestParam Long itemId) {
        return this.commonItemParamItemService.selectTbItemParamItemByItemId(itemId);
    }
}
