package com.bjsxt.common.item.controller;

import com.bjsxt.common.item.service.CommonItemParamService;
import com.bjsxt.entity.PageResult;
import com.bjsxt.feign.CommonItemParamFeignService;
import com.bjsxt.pojo.TbItemParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommonItemParamController implements CommonItemParamFeignService {
    @Autowired
    private CommonItemParamService commonItemParamService;

    @Override
    public TbItemParam selectItemParamByItemCatId(Long itemCatId) {
        return this.commonItemParamService.selectItemParamByItemCatId(itemCatId);
    }

    /**
     * 查询所有规格参数模板
     * @param page 当前页
     * @param rows 页显示数
     */
    @Override
    public PageResult selectItemParamAll(Integer page, Integer rows) {
        return this.commonItemParamService.selectItemParamAll(page, rows);
    }

    /**
     * 规格参数添加
     * @param tbItemParam 规格参数对象
     */
    @Override
    public Integer insertItemParam(@RequestBody TbItemParam tbItemParam) {
        return this.commonItemParamService.insertItemParam(tbItemParam);
    }

    /**
     * 规格参数删除
     * @param id 主键
     */
    @Override
    public Integer deleteItemParamById(Long id) {
        return this.commonItemParamService.deleteItemParamById(id);
    }
}
