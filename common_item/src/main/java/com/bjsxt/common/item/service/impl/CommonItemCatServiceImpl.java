package com.bjsxt.common.item.service.impl;

import com.bjsxt.common.item.service.CommonItemCatService;
import com.bjsxt.entity.CatNode;
import com.bjsxt.entity.CatResult;
import com.bjsxt.mapper.TbItemCatMapper;
import com.bjsxt.pojo.TbItemCat;
import com.bjsxt.pojo.TbItemCatExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommonItemCatServiceImpl implements CommonItemCatService {
    @Autowired
    private TbItemCatMapper tbItemCatMapper;

    @Override
    public List<TbItemCat> selectItemCategoryByParentId(Long parentId) {
        TbItemCatExample example = new TbItemCatExample();
        TbItemCatExample.Criteria criteria = example.createCriteria();
        criteria.andParentIdEqualTo(parentId);
        criteria.andStatusEqualTo(1);
        return this.tbItemCatMapper.selectByExample(example);
    }

    /**
     * 查询首页商品分类
     * @return CatResult
     */
    @Override
    public CatResult selectItemCategoryAll() {
        CatResult catResult = new CatResult();
        //查询商品分类
        catResult.setData(getCatList(0L));
        return catResult;
    }

    /**
     * 私有方法， 查询商品分类
     */
    private List<?> getCatList(Long parentId) {
        //创建查询条件
        TbItemCatExample example = new TbItemCatExample();
        TbItemCatExample.Criteria criteria = example.createCriteria();
        criteria.andParentIdEqualTo(parentId);
        List<TbItemCat> list = this.tbItemCatMapper.selectByExample(example);
        List resultList = new ArrayList();
        int count = 0;
        for (TbItemCat tbItemCat : list) {
            //判断是否是父节点
            if (tbItemCat.getIsParent()) {
                    CatNode catNode = new CatNode();
                    catNode.setName(tbItemCat.getName());
                    catNode.setItems(getCatList(tbItemCat.getId()));
                    resultList.add(catNode);
                    count++;
                    //只取商品分类中的 18 条数据
                    if (count == 18) {
                        break;
                    }
            } else {
                resultList.add(tbItemCat.getName());
            }
        }
        return resultList;
    }
}
