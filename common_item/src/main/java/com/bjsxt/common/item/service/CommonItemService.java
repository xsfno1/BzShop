package com.bjsxt.common.item.service;

import com.bjsxt.entity.PageResult;
import com.bjsxt.pojo.TbItem;

import java.util.Map;

public interface CommonItemService {

    PageResult selectTbItemAllByPage(Integer page, Integer rows);

    Integer insertTbItem(TbItem tbItem);

    Integer updateItemById(TbItem tbItem);

    Map<String, Object> preUpdateItem(Long itemId);

    Integer updateTbItem(TbItem tbItem);

    TbItem selectItemInfo(Long itemId);

}
