package com.bjsxt.common.item.controller;

import com.bjsxt.common.item.service.CommonItemParamItemService;
import com.bjsxt.feign.CommonItemParamItemFeignService;
import com.bjsxt.pojo.TbItemParamItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommonItemParamItemController implements CommonItemParamItemFeignService {
    @Autowired
    private CommonItemParamItemService commonItemParamItemService;
    /**
     * 数据库插入商品规格参数
     * @param tbItemParamItem 商品规格参数
     * @return Integer
     */
    @Override
    public Integer insertTbItemParamItem(@RequestBody TbItemParamItem tbItemParamItem) {
        return this.commonItemParamItemService.insertTbItemParamItem(tbItemParamItem);
    }

    /**
     * TbItemParamItem更新
     */
    @Override
    public Integer updateTbItemParamItem(@RequestBody TbItemParamItem tbItemParamItem) {
        return this.commonItemParamItemService.updateTbItemParamItem(tbItemParamItem);
    }
}
