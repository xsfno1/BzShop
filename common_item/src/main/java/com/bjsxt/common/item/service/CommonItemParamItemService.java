package com.bjsxt.common.item.service;

import com.bjsxt.pojo.TbItemParamItem;

public interface CommonItemParamItemService {
    Integer insertTbItemParamItem(TbItemParamItem tbItemParamItem);

    Integer updateTbItemParamItem(TbItemParamItem tbItemParamItem);

    TbItemParamItem selectTbItemParamItemByItemId(Long itemId);
}
