package com.bjsxt.backend.content.feign;

import com.bjsxt.backend.content.fallback.CommonContentFallbackFactory;
import com.bjsxt.feign.content.CommonContentFeignService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "common-content",fallbackFactory = CommonContentFallbackFactory.class)
public interface CommonContentFeignClient extends CommonContentFeignService {
}
