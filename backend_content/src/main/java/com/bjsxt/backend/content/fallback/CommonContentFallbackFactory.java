package com.bjsxt.backend.content.fallback;

import com.bjsxt.backend.content.feign.CommonContentFeignClient;
import com.bjsxt.entity.PageResult;
import com.bjsxt.pojo.TbContent;
import com.bjsxt.pojo.TbContentCategory;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class CommonContentFallbackFactory implements FallbackFactory<CommonContentFeignClient> {
    @Override
    public CommonContentFeignClient create(Throwable throwable) {
        return new CommonContentFeignClient() {
            @Override
            public List<TbContentCategory> selectContentCategoryByParentId(Long pid) {
                return null;
            }

            @Override
            public Integer insertContentCategory(TbContentCategory tbContentCategory) {
                return null;
            }

            @Override
            public Integer updateContentCategory(TbContentCategory tbContentCategory) {
                return null;
            }

            @Override
            public TbContentCategory selectContentCategoryById(Long id) {
                return null;
            }

            @Override
            public Integer deleteContentCategoryById(Long categoryId) {
                return null;
            }

            @Override
            public PageResult selectTbContentAllByCategoryId(Long categoryId, Integer page, Integer rows) {
                return null;
            }

            @Override
            public Integer insertTbContent(TbContent tbContent) {
                return null;
            }

            @Override
            public Integer deleteContentByIds(Long id) {
                return null;
            }

            @Override
            public List<Map> selectFrontendContentByAD() {
                return null;
            }
        };
    }
}
