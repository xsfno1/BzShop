package com.bjsxt.backend.content.service.impl;

import com.bjsxt.backend.content.feign.CommonContentFeignClient;
import com.bjsxt.backend.content.service.BackendContentService;
import com.bjsxt.entity.PageResult;
import com.bjsxt.entity.Result;
import com.bjsxt.pojo.TbContent;
import com.bjsxt.pojo.TbContentCategory;
import com.codingapi.txlcn.tc.annotation.LcnTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class BackendContentServiceImpl implements BackendContentService {
    @Autowired
    private CommonContentFeignClient commonContentFeignClient;
    /**
     * 内容分类管理查询接口
     * @param id 父节点主键 parent_id
     */
    @Override
    public Result selectContentCategoryByParentId(Long id) {
        List<TbContentCategory> list = this.commonContentFeignClient.selectContentCategoryByParentId(id);
        if(list!=null&&list.size()>0){
            return Result.ok(list);
        }
        return Result.error("查无结果");
    }
    /**
     * 内容分类管理 添加接口 ：需要同时将父节点的字段is_parent更新为1:true
     * @param parentId 父节点主键
     * @param name     内容分类名称
     */
    @Override
    @LcnTransaction
    public Result insertContentCategory(Long parentId, String name) {
        Date date = new Date();
        //补全添加对象
        TbContentCategory tbContentCategory = new TbContentCategory();
        tbContentCategory.setParentId(parentId);
        tbContentCategory.setName(name);
        tbContentCategory.setIsParent(false);
        tbContentCategory.setSortOrder(1);
        tbContentCategory.setStatus(1);
        tbContentCategory.setCreated(date);
        tbContentCategory.setUpdated(date);

        //添加
        Integer flag1 = this.commonContentFeignClient.insertContentCategory(tbContentCategory);

        //判断当前父节点是否是叶子节点，是否需要更新父节点
        TbContentCategory parent_tbContentCategory = this.commonContentFeignClient.selectContentCategoryById(parentId);
        Integer flag2 = null;
        if(!parent_tbContentCategory.getIsParent()){
            //补全父节点对象 字段is_parent更新为1:true
            parent_tbContentCategory.setIsParent(true);
            parent_tbContentCategory.setUpdated(date);

            //更新父节点字段
            flag2 = this.commonContentFeignClient.updateContentCategory(parent_tbContentCategory);
        }

        // 对下游的降级处理  相应地也需要对LCN进行配置 ==> 手动抛出异常
        if(flag1 == null || flag2 == null){
            throw new RuntimeException();
        }

        return Result.ok();
    }
    /**
     * 分类内容管理 删除接口
     * @param categoryId 父节点主键
     */
    @Override
    @LcnTransaction
    public Result deleteContentCategoryById(Long categoryId) {
        Integer deleteResult = this.commonContentFeignClient.deleteContentCategoryById(categoryId);
        if(deleteResult != null && deleteResult > 0){
            return Result.ok();
        }
        return Result.error("删除失败");
    }
    /**
     * 分类内容管理 修改接口
     * @param tbContentCategory 对象
     */
    @Override
    public Result updateContentCategory(TbContentCategory tbContentCategory) {
        //补全对象
        tbContentCategory.setUpdated(new Date());

        Integer deleteResult = this.commonContentFeignClient.updateContentCategory(tbContentCategory);
        if(deleteResult != null && deleteResult > 0){
            return Result.ok();
        }
        return Result.error("修改失败");
    }
    /**
     * 内容管理	查询接口
     * @param categoryId 分类主键
     */
    @Override
    public Result selectTbContentAllByCategoryId(Long categoryId , Integer page , Integer rows) {
        PageResult pageResult = this.commonContentFeignClient.selectTbContentAllByCategoryId(categoryId , page , rows);
        if(pageResult != null  && pageResult.getResult().size() > 0){
            return Result.ok(pageResult);
        }
        return Result.error("查无结果");
    }
    /**
     * 内容管理 添加接口
     * @param tbContent 内容对象
     */
    @Override
    public Result insertTbContent(TbContent tbContent) {
        Date date = new Date();
        //补全对象属性
        tbContent.setCreated(date);
        tbContent.setUpdated(date);

        Integer result = this.commonContentFeignClient.insertTbContent(tbContent);
        if(result != null  && result > 0){
            return Result.ok();
        }
        return Result.error("添加失败");
    }
    /**
     * 内容管理	删除接口
     * @param ids 主键
     */
    @Override
    public Result deleteContentByIds(Long ids) {
        Integer result = this.commonContentFeignClient.deleteContentByIds(ids);
        if(result != null  && result > 0){
            return Result.ok();
        }
        return Result.error("添加失败");
    }
}
