package com.bjsxt.backend.content.service;

import com.bjsxt.entity.Result;
import com.bjsxt.pojo.TbContent;
import com.bjsxt.pojo.TbContentCategory;

public interface BackendContentService {
    Result selectContentCategoryByParentId(Long id);

    Result insertContentCategory(Long parentId, String name);

    Result deleteContentCategoryById(Long categoryId);

    Result updateContentCategory(TbContentCategory tbContentCategory);

    Result selectTbContentAllByCategoryId(Long categoryId , Integer page , Integer rows);

    Result insertTbContent(TbContent tbContent);

    Result deleteContentByIds(Long ids);
}
