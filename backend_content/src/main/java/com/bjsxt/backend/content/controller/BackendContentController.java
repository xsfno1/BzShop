package com.bjsxt.backend.content.controller;

import com.bjsxt.backend.content.service.BackendContentService;
import com.bjsxt.entity.Result;
import com.bjsxt.pojo.TbContent;
import com.bjsxt.pojo.TbContentCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/content", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class BackendContentController {
    @Autowired
    private BackendContentService backendContentService;

    /**
     * 内容分类管理查询接口
     * @param id 父节点主键 parent_id
     */
    @PostMapping("/selectContentCategoryByParentId")
    public Result selectContentCategoryByParentId(@RequestParam(defaultValue = "0") Long id) {
        try {
            return this.backendContentService.selectContentCategoryByParentId(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.error("error");
    }

    /**
     * 内容分类管理 添加接口
     * @param parentId 父节点主键
     * @param name     内容分类名称
     */
    @PostMapping("/insertContentCategory")
    public Result insertContentCategory(Long parentId, String name) {
        try {
            return this.backendContentService.insertContentCategory(parentId, name);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.error("error");
    }
    /**
     * 分类内容管理 删除接口
     * @param categoryId 父节点主键
     */
    @PostMapping("/deleteContentCategoryById")
    public Result deleteContentCategoryById(Long categoryId) {
        try {
            return this.backendContentService.deleteContentCategoryById(categoryId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.error("error");
    }
    /**
     * 分类内容管理 修改接口
     * @param tbContentCategory 对象
     */
    @PostMapping("/updateContentCategory")
    public Result updateContentCategory(TbContentCategory tbContentCategory) {
        try {
            return this.backendContentService.updateContentCategory(tbContentCategory);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.error("error");
    }
    /**
     * 内容管理	查询接口
     * @param categoryId 分类主键
     */
    @PostMapping("/selectTbContentAllByCategoryId")
    public Result selectTbContentAllByCategoryId(Long categoryId , @RequestParam(defaultValue = "1") Integer page ,
                                                 @RequestParam(defaultValue = "30") Integer rows) {
        try {
            return this.backendContentService.selectTbContentAllByCategoryId(categoryId , page , rows);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.error("error");
    }
    /**
     * 内容管理 添加接口
     * @param tbContent 内容对象
     */
    @PostMapping("/insertTbContent")
    public Result insertTbContent(TbContent tbContent) {
        try {
            return this.backendContentService.insertTbContent(tbContent);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.error("error");
    }
    /**
     * 内容管理	删除接口
     * @param ids 主键
     */
    @PostMapping("/deleteContentByIds")
    public Result deleteContentByIds(Long ids) {
        try {
            return this.backendContentService.deleteContentByIds(ids);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.error("error");
    }
}
