package com.bjsxt.backend.content;

import com.codingapi.txlcn.tc.config.EnableDistributedTransaction;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableDistributedTransaction   //Tx-LCN
@EnableFeignClients
public class BackContentApp {
    public static void main(String[] args) {
        SpringApplication.run(BackContentApp.class,args);
    }
}
